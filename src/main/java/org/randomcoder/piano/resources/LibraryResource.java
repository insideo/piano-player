package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.Library;

@Path("/library") public class LibraryResource {

  @Inject public TrackLibrary library;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Library getLibrary(@Context UriInfo uriInfo) {
    return new Library(uriInfo);
  }

  @POST @Path("refresh!") @Produces(MediaType.APPLICATION_JSON)
  public Response refresh(@Context UriInfo uriInfo) throws Exception {
    library.refresh();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/library").build()).build();
  }

}
