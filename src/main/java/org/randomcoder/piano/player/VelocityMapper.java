package org.randomcoder.piano.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class VelocityMapper {

  private static final Logger LOG =
      LoggerFactory.getLogger(VelocityMapper.class);

  private static final int VELOCITY_SIZE = 2 << 6;
  private static final int MIN_VELOCITY = 1;
  private static final int MAX_VELOCITY = VELOCITY_SIZE - 1;

  private final int[] minKeyVelocities;
  private final double minVolume;
  private final double maxVolume;
  private final int maxKeyVelocity;
  private final int velocityPivotPoint;
  private final int velocityLowRange;

  private volatile double volume;
  private volatile double scaledVolume;

  public VelocityMapper(int[] minKeyVelocities, double minVolume,
      double maxVolume, double defaultVolume, int maxKeyVelocity,
      int velocityPivotPoint, int velocityLowRange) {

    this.minKeyVelocities =
        Arrays.copyOf(minKeyVelocities, minKeyVelocities.length);
    this.minVolume = minVolume;
    this.maxVolume = maxVolume;
    this.volume = defaultVolume;
    this.scaledVolume = scaledVolume(defaultVolume);
    this.maxKeyVelocity = maxKeyVelocity;
    this.velocityPivotPoint = velocityPivotPoint;
    this.velocityLowRange = velocityLowRange;
  }

  public int map(int note, int velocity) {
    // velocity of zero means key off
    if (velocity == 0) {
      return 0;
    }

    // normalize incoming velocity to acceptable range
    if (velocity < MIN_VELOCITY) {
      velocity = MIN_VELOCITY;
    }
    if (velocity > MAX_VELOCITY) {
      velocity = MAX_VELOCITY;
    }

    int minKeyVelocity = minKeyVelocities[note];

    double mapped =
        (velocity < velocityPivotPoint) ? belowPivot(minKeyVelocity, velocity) :
            abovePivot(minKeyVelocity, velocity);

    // clip
    int result = (int) Math.round(mapped);
    LOG.trace("after rounding: {}", result);

    result = Math.min(maxKeyVelocity, Math.max(minKeyVelocity, result));

    LOG.debug("map(note={}, velocity={}): {}", note, velocity, result);

    return result;
  }

  public double getVolume() {
    return volume;
  }

  public void setVolume(double value) {
    value = Math.max(0.0d, Math.min(value, 1.0d));
    double scaled = scaledVolume(value);
    this.volume = value;
    this.scaledVolume = scaled;
  }

  double belowPivot(int minKeyVelocity, int velocity) {
    LOG.trace("below pivot, mapping to range: {} -> {}", minKeyVelocity,
        minKeyVelocity + velocityLowRange);

    // ratio is delta-output / delta-input
    double ratio =
        (double) velocityLowRange / (double) (velocityPivotPoint - 2);
    LOG.trace("below-pivot ratio: {}", ratio);

    double scaled = ((double) (velocity - 1) * ratio) + minKeyVelocity;
    LOG.trace("scaled: {}", scaled);

    return scaled;
  }

  double abovePivot(int minKeyVelocity, int velocity) {
    LOG.trace("above pivot, mapping to range: {} -> {}",
        minKeyVelocity + velocityLowRange, MAX_VELOCITY);

    int inputRange = MAX_VELOCITY - velocityPivotPoint;
    LOG.trace("input range: {}", inputRange);

    int outputRange = MAX_VELOCITY - minKeyVelocity - velocityLowRange;
    LOG.trace("output range: {}", outputRange);

    int offsetVelocity = velocity - velocityPivotPoint;
    LOG.trace("offset velocity: {}", offsetVelocity);

    double scaled =
        (inputRange < 1) ? 0 : (double) offsetVelocity / (double) inputRange;
    LOG.trace("scaled: {}", scaled);

    // apply volume scaling
    double scaledWithVolume = scaled * scaledVolume;
    LOG.trace("scaled with volume: {}", scaledWithVolume);

    double outputScaled = scaledWithVolume * (double) outputRange;
    LOG.trace("outputScaled: {}", outputScaled);

    // map to output range
    double outputValue =
        (double) (minKeyVelocity + velocityLowRange) + outputScaled;
    LOG.trace("outputValue: {}", outputValue);

    return outputValue;
  }

  double scaledVolume(double value) {
    return minVolume + (value * (maxVolume - minVolume));
  }

}
