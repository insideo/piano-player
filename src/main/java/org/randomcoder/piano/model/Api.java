package org.randomcoder.piano.model;

import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.Map;

public class Api {

  public Map<String, URI> links;

  public final String version = "1.0";

  public Api(UriInfo uriInfo) {
    this.links =
        Map.of("library", uriInfo.getBaseUriBuilder().path("/library").build(),
            "category",
            uriInfo.getBaseUriBuilder().path("/library/category").build(),
            "composer",
            uriInfo.getBaseUriBuilder().path("/library/composer").build(),
            "performer",
            uriInfo.getBaseUriBuilder().path("/library/performer").build(),
            "playlist", uriInfo.getBaseUriBuilder().path("/playlist").build(),
            "nowplaying",
            uriInfo.getBaseUriBuilder().path("/now-playing").build(), "mixer",
            uriInfo.getBaseUriBuilder().path("/mixer").build());
  }

}
