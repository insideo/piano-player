import 'onsenui/css/onsenui.css';
//import 'onsenui/css/onsen-css-components.css';
import 'onsenui/css/dark-onsen-css-components.css';

import Vue from 'vue'
import Vuex from 'vuex'
import VueOnsen from 'vue-onsenui';
import store from './store'
import App from './App.vue';

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueOnsen)

new Vue({
  el: '#app',
  render: h => h(App),
  store,
  beforeCreate() {
    Vue.prototype.md = this.$ons.platform.isAndroid();
    if (this.$ons.platform.isIPhoneX()) {
      document.documentElement.setAttribute('onsflag-iphonex-portrait', '');
      document.documentElement.setAttribute('onsflag-iphonex-landscape', '');
    }
  }, created() {
    this.$store.dispatch('init')
  }
});
