package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@JsonInclude(value = Include.NON_NULL) public class Composer
    implements Comparable<Composer> {

  public String id;
  public String name;

  @JsonInclude(value = Include.NON_EMPTY) public List<Track> tracks =
      new ArrayList<>();

  @JsonInclude(value = Include.NON_EMPTY) public Map<String, URI> links =
      new HashMap<>();

  public Composer() {
  }

  public Composer(String id) {
    this.id = id;
  }

  public Composer(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public boolean missing() {
    return "-".equals(id);
  }

  @Override public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Composer other = (Composer) obj;
    return Objects.equals(id, other.id);
  }

  @Override public int compareTo(Composer o) {
    int result = Boolean.compare(missing(), o.missing());
    if (result != 0) {
      return result;
    }
    result = Objects.compare(name, o.name, String::compareToIgnoreCase);
    if (result != 0) {
      return result;
    }
    return Objects.compare(id, o.id, String::compareTo);
  }

  public Composer metaClone() {
    return new Composer(id, name);
  }

  public Composer withLinks(UriInfo info) {
    Composer clone = new Composer(id, name);
    clone.tracks.addAll(
        tracks.stream().map(c -> c.withLinks(info, TrackLinkContext.LIBRARY))
            .collect(Collectors.toList()));

    clone.links.put("self",
        info.getBaseUriBuilder().path("/library/composer/{id}").build(id));
    clone.links.put("enqueue",
        info.getBaseUriBuilder().path("/library/composer/{id}/enqueue!")
            .build(id));
    clone.links.put("dequeue",
        info.getBaseUriBuilder().path("/library/composer/{id}/dequeue!")
            .build(id));
    return clone;
  }

}
