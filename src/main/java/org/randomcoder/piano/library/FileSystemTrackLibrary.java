package org.randomcoder.piano.library;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import jakarta.ws.rs.NotFoundException;
import org.randomcoder.piano.model.Category;
import org.randomcoder.piano.model.Composer;
import org.randomcoder.piano.model.Performer;
import org.randomcoder.piano.model.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class FileSystemTrackLibrary implements TrackLibrary {

  private static final Logger LOG =
      LoggerFactory.getLogger(FileSystemTrackLibrary.class);

  private final Path libraryPath;
  private final Path libraryMetadataCache;
  private final Path doorbellMidi;
  private final double doorbellVolume;
  private final AtomicReference<State> stateHolder;
  private final ObjectMapper mapper;
  private final ObjectMapper cacheMapper;

  public FileSystemTrackLibrary(Path libraryPath, Path libraryMetadataCache,
      Path doorbellMidi, double doorbellVolume) {
    this.libraryPath = libraryPath;
    this.libraryMetadataCache = libraryMetadataCache;
    this.doorbellMidi = doorbellMidi;
    this.doorbellVolume = doorbellVolume;
    this.stateHolder =
        new AtomicReference<FileSystemTrackLibrary.State>(new State());
    this.mapper = new ObjectMapper(new YAMLFactory());
    this.cacheMapper = new ObjectMapper();
  }

  static MessageDigest sha1() {
    try {
      return MessageDigest.getInstance("SHA-1");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("Unable to setup SHA-1 digest", e);
    }
  }

  @Override public void refresh() throws IOException {
    LOG.info("Refreshing library from {}...", libraryPath);

    long start, end, elapsed;

    var state = new State();
    var metaCache = metadataCache();
    var metadata = new MultiTrackMetadata();

    // read metadata collections
    start = System.currentTimeMillis();
    Files.find(libraryPath, 3,
        (p, a) -> a.isRegularFile() && p.getFileName().toString()
            .equals("metadata.yml")).sequential()
        .forEach(p -> processMetadata(metadata, p));
    end = System.currentTimeMillis();
    elapsed = end - start;
    LOG.info("Refreshed track metadata. Found {} tracks in {}ms.",
        metadata.files.size(), elapsed);
    if (LOG.isTraceEnabled()) {
      metadata.files.entrySet()
          .forEach(e -> LOG.trace("  {} -> {}", e.getKey(), e.getValue()));
    }

    // process files
    start = System.currentTimeMillis();
    Files.find(libraryPath, 3,
        (p, a) -> a.isRegularFile() && p.getFileName().toString()
            .endsWith("mid")).sequential()
        .forEach(p -> processFile(state, metadata, metaCache, p));

    // read doorbell track
    state.doorbellTrack = readDoorbellTrack();

    state.done();

    end = System.currentTimeMillis();
    elapsed = end - start;

    LOG.info(
        "Refreshed library. Found {} tracks, {} composers, {} performers, and {} categories in {}ms.",
        state.trackIndex.size(), state.composers.size(),
        state.performers.size(), state.categories.size(), elapsed);

    Duration playTime = Duration.ofSeconds(
        state.trackIndex.values().stream().filter(t -> t.lengthMicros != null)
            .mapToLong(t -> t.lengthMicros).sum() / 1_000_000L);

    LOG.info("Total play time: {}", String
        .format("%02d:%02d:%02d", playTime.toHours(), playTime.toMinutesPart(),
            playTime.toSecondsPart()));

    saveMetadataCache(state.createMetadataCache());
    stateHolder.set(state);
  }

  static String sha1(String value) {
    byte[] digest = sha1().digest(value.getBytes(StandardCharsets.UTF_8));
    return HexFormat.of().withLowerCase().formatHex(digest);
  }

  void processMetadata(MultiTrackMetadata metadata, Path metadataFile) {
    LOG.debug("Processing metadata file {}", metadataFile);

    if (!Files.isRegularFile(metadataFile)) {
      return;
    }

    Path dirName = libraryPath.relativize(metadataFile).getParent();
    String relPath = dirName == null ? "" : dirName.toString() + "/";
    LOG.trace("Calculated relative path for metadata: {}", relPath);

    try (var mis = Files.newInputStream(metadataFile)) {
      var current = mapper.readValue(mis, MultiTrackMetadata.class);
      metadata.apply(relPath, current);
    } catch (Exception e) {
      LOG.warn("Unable to process metadata file {}", metadataFile, e);
    }
  }

  void processFile(State state, MultiTrackMetadata metadata,
      MetadataCache metaCache, Path midiFile) {
    try {
      LOG.trace("Processing MIDI file {}", midiFile);

      String shortPath = libraryPath.relativize(midiFile).toString();
      String id = sha1(shortPath);

      long lengthMicros = trackLengthMicros(id, metaCache, midiFile);

      TrackMetadata meta = metadata.files.get(shortPath);
      if (meta == null) {
        LOG.warn("Missing metadata for track {}", midiFile);
        meta = new TrackMetadata();
      } else {
        LOG.trace("Track metadata: {}", meta);
      }

      String title =
          meta.title().orElseGet(() -> midiFile.getFileName().toString());
      String sortTitle = meta.sortTitle;
      Composer composer = meta.composer().map(state::composer)
          .orElseGet(state::unknownComposer);
      Performer performer = meta.performer().map(state::performer)
          .orElseGet(state::unknownPerformer);
      Category category =
          meta.category().map(state::category).orElseGet(state::uncategorized);
      Integer performanceYear = meta.performanceYear;
      Track track =
          new Track(id, midiFile, title, sortTitle, composer, performer,
              category, performanceYear, lengthMicros);
      state.addTrack(track);
    } catch (Exception e) {
      LOG.warn("Unable to process midi file {}", midiFile, e);
    }
  }

  long trackLengthMicros(String id, MetadataCache metaCache, Path midiFile)
      throws IOException, InvalidMidiDataException {
    if (metaCache != null) {
      Long cachedResult = metaCache.trackLengths.get(id);
      if (cachedResult != null) {
        LOG.trace("Returning cached track length for {}", midiFile);
        return cachedResult;
      }
    }
    try (var is = Files.newInputStream(midiFile);
        var bis = new BufferedInputStream(is)) {
      return MidiSystem.getSequence(bis).getMicrosecondLength();
    }
  }

  MultiTrackMetadata multiMeta(Path metadataFile) {
    if (!Files.isRegularFile(metadataFile)) {
      return new MultiTrackMetadata();
    }

    try (var mis = Files.newInputStream(metadataFile)) {
      return mapper.readValue(mis, MultiTrackMetadata.class);
    } catch (IOException e) {
      LOG.warn("Unable to read track metadata from {}", metadataFile, e);
      return new MultiTrackMetadata();
    }
  }

  @Override public List<Category> listCategories() {
    return stateHolder.get().categories;
  }

  @Override public List<Composer> listComposers() {
    return stateHolder.get().composers;
  }

  @Override public List<Performer> listPerformers() {
    return stateHolder.get().performers;
  }

  @Override public Track fetchTrack(String trackId) {
    return Optional.ofNullable(stateHolder.get().trackIndex.get(trackId))
        .orElseThrow(NotFoundException::new);
  }

  @Override public Track getDoorbellTrack() {
    return stateHolder.get().doorbellTrack;
  }

  @Override public double getDoorbellVolume() {
    return doorbellVolume;
  }

  @Override public Category fetchCategory(String categoryId) {
    return Optional.ofNullable(stateHolder.get().categoryIndex.get(categoryId))
        .orElseThrow(NotFoundException::new);
  }

  @Override public Composer fetchComposer(String composerId) {
    return Optional.ofNullable(stateHolder.get().composerIndex.get(composerId))
        .orElseThrow(NotFoundException::new);
  }

  @Override public Performer fetchPerformer(String performerId) {
    return Optional
        .ofNullable(stateHolder.get().performerIndex.get(performerId))
        .orElseThrow(NotFoundException::new);
  }

  static class State {

    final Map<String, Track> trackIndex = new HashMap<>();
    final Map<String, Category> categoryIndex = new HashMap<>();
    final Map<String, Composer> composerIndex = new HashMap<>();
    final Map<String, Performer> performerIndex = new HashMap<>();

    final Map<Category, Category> categoryMeta = new HashMap<>();
    final Map<Composer, Composer> composerMeta = new HashMap<>();
    final Map<Performer, Performer> performerMeta = new HashMap<>();

    List<Category> categories;
    List<Composer> composers;
    List<Performer> performers;

    Track doorbellTrack;

    Composer composer(String name) {
      String id = sha1(name);
      return composerIndex.computeIfAbsent(id, s -> new Composer(id, name));
    }

    Composer unknownComposer() {
      return composerIndex
          .computeIfAbsent("-", s -> new Composer(s, "Unknown"));
    }

    Performer performer(String name) {
      String id = sha1(name);
      return performerIndex.computeIfAbsent(id, s -> new Performer(id, name));
    }

    Performer unknownPerformer() {
      return performerIndex
          .computeIfAbsent("-", s -> new Performer(s, "Unknown"));
    }

    Category category(String name) {
      String id = sha1(name);
      return categoryIndex.computeIfAbsent(id, s -> new Category(id, name));
    }

    Category uncategorized() {
      return categoryIndex
          .computeIfAbsent("-", s -> new Category(s, "Uncategorized"));
    }

    void addTrack(Track track) {
      trackIndex.put(track.id, track);
      track.category.tracks.add(track);
      track.composer.tracks.add(track);
      track.performer.tracks.add(track);
      track.category =
          categoryMeta.computeIfAbsent(track.category, Category::metaClone);
      track.composer =
          composerMeta.computeIfAbsent(track.composer, Composer::metaClone);
      track.performer =
          performerMeta.computeIfAbsent(track.performer, Performer::metaClone);
    }

    void done() {
      // sort tracks within groups
      categoryIndex.values().stream().forEach(c -> Collections.sort(c.tracks));
      composerIndex.values().stream().forEach(c -> Collections.sort(c.tracks));
      performerIndex.values().stream().forEach(p -> Collections.sort(p.tracks));

      // build metadata lists
      categories = categoryMeta.values().stream().collect(Collectors.toList());
      composers = composerMeta.values().stream().collect(Collectors.toList());
      performers = performerMeta.values().stream().collect(Collectors.toList());

      // sort metadata lists
      Collections.sort(categories);
      Collections.sort(composers);
      Collections.sort(performers);
    }

    MetadataCache createMetadataCache() {
      var cache = new MetadataCache();
      for (var track : trackIndex.values()) {
        cache.trackLengths.put(track.id, track.lengthMicros);
      }
      return cache;
    }
  }

  synchronized void saveMetadataCache(MetadataCache metadataCache) {
    if (libraryMetadataCache == null) {
      return;
    }
    try (var mos = Files.newOutputStream(libraryMetadataCache)) {
      cacheMapper.writeValue(mos, metadataCache);
      LOG.info("Saved metadata cache to {}", libraryMetadataCache);
    } catch (IOException e) {
      LOG.warn("Unable to write metadata cache to {}", libraryMetadataCache, e);
    }
  }

  Track readDoorbellTrack() {
    if (doorbellMidi == null) {
      return null;
    }

    try {
      long len;
      try (var is = Files.newInputStream(doorbellMidi);
          var bis = new BufferedInputStream(is)) {
        len = MidiSystem.getSequence(bis).getMicrosecondLength();
      }
      LOG.debug("Read doorbell track from {} of length {} microseconds",
          doorbellMidi, len);

      return new Track("doorbell", doorbellMidi, "doorbell", null, null, null,
          null, null, len);
    } catch (Exception e) {
      LOG.warn("Unable to read doorbell track from {}", doorbellMidi, e);
      return null;
    }
  }

  MetadataCache metadataCache() {
    if (libraryMetadataCache == null || !Files
        .isRegularFile(libraryMetadataCache)) {
      return null;
    }
    try (var mis = Files.newInputStream(libraryMetadataCache)) {
      var cache = cacheMapper.readValue(mis, MetadataCache.class);
      return cache.isCurrent() ? cache : null;
    } catch (IOException e) {
      LOG.warn("Unable to read metadata cache from {}", libraryMetadataCache,
          e);
      return null;
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class MetadataCache {
    public static final String CURRENT_VERSION = "1";

    public String version = CURRENT_VERSION;
    public Map<String, Long> trackLengths = new HashMap<>();

    @JsonIgnore public boolean isCurrent() {
      return CURRENT_VERSION.equals(version);
    }

    @Override public String toString() {
      return String
          .format("{ version=%s, trackLengths=%s }", version, trackLengths);
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class MultiTrackMetadata {
    public Map<String, TrackMetadata> files = new HashMap<>();

    public void apply(String prefix, MultiTrackMetadata meta) {
      meta.files.entrySet().stream().sequential()
          .forEach(e -> files.put(prefix + e.getKey(), e.getValue()));
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class TrackMetadata {

    public String title;
    public String sortTitle;
    public String performer;
    public String composer;
    public String category;
    public Integer performanceYear;

    public Optional<String> title() {
      return Optional.ofNullable(title);
    }

    public Optional<String> composer() {
      return Optional.ofNullable(composer);
    }

    public Optional<String> performer() {
      return Optional.ofNullable(performer);
    }

    public Optional<String> category() {
      return Optional.ofNullable(category);
    }

    @Override public String toString() {
      return String.format(
          "{ title=%s, sortTitle=%s, performer=%s, composer=%s, category=%s, performanceYear=%d }",
          title, sortTitle, performer, composer, category, performanceYear);
    }

  }

}
