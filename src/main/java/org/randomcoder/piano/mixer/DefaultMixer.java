package org.randomcoder.piano.mixer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class DefaultMixer implements Mixer {

  private static final Logger LOG = LoggerFactory.getLogger(DefaultMixer.class);

  private final AtomicReference<Double> currentVolume;

  private final CopyOnWriteArrayList<Consumer<VolumeChangedEvent>>
      volumeChangedListeners;

  public DefaultMixer(double defaultVolume) {
    this.currentVolume = new AtomicReference<>(defaultVolume);
    this.volumeChangedListeners = new CopyOnWriteArrayList<>();
  }

  @Override
  public void addVolumeChangedListener(Consumer<VolumeChangedEvent> listener) {
    volumeChangedListeners.add(listener);
  }

  @Override public double getVolume() {
    return currentVolume.get();
  }

  public void setVolume(double value) {
    double before = currentVolume.get();

    if (value < 0d) {
      value = 0d;
    }
    if (value > 1d) {
      value = 1d;
    }

    LOG.info("Current volume level: {}", value * 100);

    // notify listeners
    var event = new VolumeChangedEvent(before, value);
    volumeChangedListeners.stream().forEach(c -> c.accept(event));

    // update internal state
    currentVolume.set(value);
  }

}
