package org.randomcoder.piano.player;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.JVM) public class VelocityMapperTest {

  @Rule public TestName name = new TestName();

  static int[] minKeyVelocities = new int[128];

  @BeforeClass public static void setUpBeforeClass() {
    for (int i = 0; i < minKeyVelocities.length; i++) {
      minKeyVelocities[i] = i;
    }
  }

  VelocityMapper scaled;
  VelocityMapper volFloor;
  VelocityMapper pivot;
  VelocityMapper clipped;

  @Before public void setUp() {
    scaled = new VelocityMapper(minKeyVelocities, 0.0d, 2.0d, 0.5d, 127, 32, 5);
    volFloor =
        new VelocityMapper(minKeyVelocities, 0.25d, 2.0d, 0.0d, 127, 32, 5);
    clipped =
        new VelocityMapper(minKeyVelocities, 0.0d, 1.0d, 0.5d, 100, 32, 5);
    pivot = new VelocityMapper(minKeyVelocities, 0.0d, 1.0d, 1.0d, 127, 32, 5);
    System.err.printf("%n======== %s ========%n", name.getMethodName());
  }

  @Test public void scaledVolumeShouldReturnLowForZero() {
    double result = volFloor.scaledVolume(0d);
    System.out.printf("scaledVolume(0d) => %f%n", result);
    assertEquals(0.25d, result, 0.0001d);
  }

  @Test public void scaledVolumeShouldReturnHighForOne() {
    double result = scaled.scaledVolume(1d);
    System.out.printf("scaledVolume(1d) => %f%n", result);
    assertEquals(2.0d, result, 0.0001d);
  }

  @Test public void pivotAtLowRangeShouldMapToMinKeyVelocity() {
    assertEquals(40, pivot.map(40, 1));
  }

  @Test
  public void scaledAtLowRangeShouldMapToMinKeyVelocityIrrespectiveOfVolume() {
    assertEquals(40, scaled.map(40, 1));
  }

  @Test public void pivotJustBeforePivotPointShouldMapToTopOfLowRange() {
    assertEquals(45, pivot.map(40, 31));
  }

  @Test
  public void scaledJustBeforePivotPointShouldMapToTopOfLowRangeIrrespectiveOfVolume() {
    assertEquals(45, scaled.map(40, 31));
  }

  @Test public void pivotAtPivotPointShouldMapToPivotPoint() {
    assertEquals(45, pivot.map(40, 32));
  }

  @Test public void scaledAtPivotPointShouldMapToPivotPointAtDefaultVolume() {
    assertEquals(45, scaled.map(40, 32));
  }

  @Test public void scaledAtPivotPointShouldMapToPivotPointWhenVolumeIsMin() {
    scaled.setVolume(0d);
    assertEquals(45, scaled.map(40, 32));
  }

  @Test public void scaledAtPivotPointShouldMapToPivotPointWhenVolumeIsMax() {
    scaled.setVolume(1d);
    assertEquals(45, scaled.map(40, 32));
  }

  @Test public void volFloorAtPivotPointShouldMapToPivotPointWhenVolumeIsMin() {
    volFloor.setVolume(0d);
    assertEquals(45, volFloor.map(40, 32));
  }

  @Test public void volFloorAtPivotPointShouldMapToPivotPointWhenVolumeIsMax() {
    volFloor.setVolume(1d);
    assertEquals(45, volFloor.map(40, 32));
  }

  @Test public void pivotAtMaxShouldMapToMax() {
    assertEquals(127, pivot.map(40, 127));
  }

  @Test public void scaledAtMaxShouldMapToMaxAtDefaultVolume() {
    assertEquals(127, scaled.map(40, 127));
  }

  @Test public void scaledAtMaxShouldMapToLowRangeAtMinVolume() {
    scaled.setVolume(0d);
    assertEquals(45, scaled.map(40, 127));
  }

  @Test public void scaledAtMaxShouldMapToMaxAtMaxVolume() {
    scaled.setVolume(1d);
    assertEquals(127, scaled.map(40, 127));
  }

  @Test public void clippedAtMaxShouldMapToLowRangeAtMinVolume() {
    clipped.setVolume(0d);
    assertEquals(27 + 5, clipped.map(27, 127));
  }

  @Test
  public void clippedAtMaxShouldMapToMiddleOfOutputRangeAtDefaultVolume() {
    assertEquals(27 + 5 + Math.round(95 * .5), clipped.map(27, 127));
  }

  @Test
  public void volFloorAtMidrangeShouldMapTo25PercentFromBaselineWhenVolumeIsMin() {
    volFloor.setVolume(0d);
    assertEquals(27 + 5 + Math.round((95 - (127 - 80)) * .25),
        volFloor.map(27, 80));
  }

  @Test
  public void volFloorAtMaxShouldMapTo25PercentFromBaselineWhenVolumeIsMin() {
    volFloor.setVolume(0d);
    assertEquals(27 + 5 + Math.round(95 * .25), volFloor.map(27, 127));
  }

}
