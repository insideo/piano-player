package org.randomcoder.piano.player;

import org.randomcoder.piano.model.NowPlaying;
import org.randomcoder.piano.model.Track;

public interface Player {

  public NowPlaying getCurrentState();

  public void play();

  public void stop();

  public void seekPrev();

  public void seekNext();

  public void forward30();

  public void back30();

  public void seek(Track track);

  public void startup() throws Exception;

  public void shutdown() throws Exception;

  public void playDoorbellTrack(Track track, double volume);

}
