package org.randomcoder.piano.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CalibrationData {

  public List<KeyCalibration> keyCalibrations = new ArrayList<>();

  @JsonIgnore public Map<Integer, KeyCalibration> getKeyCalibrationsMap() {
    Map<Integer, KeyCalibration> map = new TreeMap<>();
    for (var key : keyCalibrations) {
      map.put(key.key, key);
    }
    return map;
  }

  public CalibrationData() {
  }

  public CalibrationData(List<KeyCalibration> keyCalibrations) {
    this.keyCalibrations = keyCalibrations;
  }

  public CalibrationData(Map<Integer, KeyCalibration> keyCalibrations) {
    this.keyCalibrations.addAll(keyCalibrations.values());
  }
}
