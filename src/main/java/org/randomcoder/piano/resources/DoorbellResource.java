package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.player.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/doorbell") public class DoorbellResource {

  private static final Logger LOG =
      LoggerFactory.getLogger(DoorbellResource.class);

  @Inject public Player player;

  @Inject public TrackLibrary library;

  @POST @Produces(MediaType.APPLICATION_JSON)
  public Response ringDoorbell(@Context UriInfo uriInfo) {
    LOG.info("Doorbell is ringing");
    player.playDoorbellTrack(library.getDoorbellTrack(),
        library.getDoorbellVolume());
    return Response.noContent().build();
  }

  @Path("/heartbeat")
  @POST @Produces(MediaType.APPLICATION_JSON)
  public Response doorbellHeartbeat(@Context UriInfo uriInfo) {
    LOG.info("Doorbell is alive");
    return Response.noContent().build();
  }

}
