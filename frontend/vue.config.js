const CompressionPlugin = require("compression-webpack-plugin")

module.exports = {
  devServer: {
    proxy: {
      '/api/v1': {
        target: 'http://localhost:8081/',
        ws: true,
        changeOrigin: true
      }
    }
  },
  configureWebpack: {
    plugins: [
      new CompressionPlugin({
        exclude: [
          /.*\.ico/
        ]
      })
    ]
  }
}
