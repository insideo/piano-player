module org.randomcoder.piano {
    requires java.base;
    requires java.desktop;
    requires java.logging;

    requires ch.qos.logback.classic;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.yaml;
    requires com.fasterxml.jackson.module.jakarta.xmlbind;
    requires jakarta.annotation;
    requires jakarta.inject;
    requires jakarta.ws.rs;
    requires jakarta.xml.bind;
    requires jakarta.validation;
    requires jersey.container.servlet.core;
    requires jersey.common;
    requires jersey.hk2;
    requires jersey.server;
    requires jul.to.slf4j;
    requires org.glassfish.hk2.api;
    requires org.glassfish.hk2.utilities;
    requires org.eclipse.jetty.http2.server;
    requires org.eclipse.jetty.rewrite;
    requires org.eclipse.jetty.server;
    requires org.eclipse.jetty.servlet;
    requires org.slf4j;

    opens org.randomcoder.piano;
    opens org.randomcoder.piano.config;
    opens org.randomcoder.piano.library;
    opens org.randomcoder.piano.mixer;
    opens org.randomcoder.piano.model;
    opens org.randomcoder.piano.player;
    opens org.randomcoder.piano.playlist;
    opens org.randomcoder.piano.providers;
    opens org.randomcoder.piano.resources;
    opens org.randomcoder.piano.util;

    opens webapp;
    opens webapp.css;
    opens webapp.fonts;
    opens webapp.img;
    opens webapp.js;
}
