package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.Categories;
import org.randomcoder.piano.model.Category;
import org.randomcoder.piano.playlist.PlaylistState;

@Path("/library/category") public class CategoryResource {

  @Inject public TrackLibrary library;

  @Inject public PlaylistState playlistState;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Categories listCategories(@Context UriInfo uriInfo) {
    return new Categories(library.listCategories(), uriInfo);
  }

  @GET @Path("{id}") @Produces(MediaType.APPLICATION_JSON)
  public Category getCategory(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    return library.fetchCategory(id).withLinks(uriInfo);
  }

  @POST @Path("{id}/enqueue!") @Consumes(MediaType.WILDCARD)
  public Response enqueueCategory(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.add(library.fetchCategory(id).tracks);
    return Response.noContent().build();
  }

  @POST @Path("{id}/dequeue!") @Consumes(MediaType.WILDCARD)
  public Response dequeueTrack(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.remove(library.fetchCategory(id).tracks);
    return Response.noContent().build();
  }

}
