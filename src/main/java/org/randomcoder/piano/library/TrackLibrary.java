package org.randomcoder.piano.library;

import org.randomcoder.piano.model.Category;
import org.randomcoder.piano.model.Composer;
import org.randomcoder.piano.model.Performer;
import org.randomcoder.piano.model.Track;

import java.io.IOException;
import java.util.List;

public interface TrackLibrary {

  public void refresh() throws IOException;

  public List<Category> listCategories();

  public List<Composer> listComposers();

  public List<Performer> listPerformers();

  public Track fetchTrack(String trackId);

  public Category fetchCategory(String categoryId);

  public Composer fetchComposer(String composerId);

  public Performer fetchPerformer(String performerId);

  public Track getDoorbellTrack();

  public double getDoorbellVolume();
}
