package org.randomcoder.piano.player;

import org.randomcoder.piano.mixer.Mixer;
import org.randomcoder.piano.mixer.VolumeChangedEvent;
import org.randomcoder.piano.model.NowPlaying;
import org.randomcoder.piano.model.Track;
import org.randomcoder.piano.playlist.PlaylistClearedEvent;
import org.randomcoder.piano.playlist.PlaylistShuffledEvent;
import org.randomcoder.piano.playlist.PlaylistState;
import org.randomcoder.piano.playlist.PlaylistTracksAddedEvent;
import org.randomcoder.piano.playlist.PlaylistTracksRemovedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.Transmitter;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class DefaultPlayer implements Player {

  private final static Logger LOG =
      LoggerFactory.getLogger(DefaultPlayer.class);

  private final MidiDevice midiDevice;
  private final PlaylistState playlistState;
  private final MidiPlayerThread playerThread;

  private final AtomicReference<NowPlaying> doorbellState =
      new AtomicReference<>();
  private final AtomicReference<Double> doorbellVolume =
      new AtomicReference<>();

  public DefaultPlayer(MidiDevice midiDevice, PlaylistState playlistState,
      Mixer mixer, int[] minKeyVelocities, double minVolume, double maxVolume,
      double defaultVolume, int maxKeyVelocity, int velocityPivotPoint,
      int velocityLowRange) {

    this.midiDevice = midiDevice;
    this.playlistState = playlistState;
    this.playerThread = new MidiPlayerThread(
        new VelocityMapper(minKeyVelocities, minVolume, maxVolume,
            defaultVolume, maxKeyVelocity, velocityPivotPoint,
            velocityLowRange));

    playlistState.addPlaylistClearedListener(this::playlistCleared);
    playlistState.addPlaylistShuffledListener(this::playlistShuffled);
    playlistState.addPlaylistTracksAddedListener(this::tracksAdded);
    playlistState.addPlaylistTracksRemovedListener(this::tracksRemoved);
    mixer.addVolumeChangedListener(this::volumeChanged);
  }

  @Override public void startup() throws Exception {
    playerThread.start();
    playerThread.init();
  }

  @Override public void shutdown() throws Exception {
    playerThread.shutdown();
  }

  void volumeChanged(VolumeChangedEvent event) {
    LOG.debug("Volume scale changed: {}", event.getAfter());
    playerThread.setVolume(event.getAfter());
  }

  void playlistCleared(PlaylistClearedEvent event) {
    LOG.debug("Playlist cleared");
    playerThread.closeTrack();
  }

  void playlistShuffled(PlaylistShuffledEvent event) {
    LOG.debug("Playlist shuffled");

    NowPlaying state = playerThread.getCurrentState();

    if (event.getAfter().tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    Track nextTrack = event.getAfter().tracks.get(0);
    try {
      playerThread.openTrack(nextTrack);
    } catch (Exception e) {
      LOG.error("Unable to open track {}", nextTrack.filename, e);
      return;
    }
    if (state.playing) {
      playerThread.startPlayback();
    }
  }

  void tracksAdded(PlaylistTracksAddedEvent event) {
    LOG.debug("Tracks added");

    NowPlaying state = playerThread.getCurrentState();

    if (event.getAfter().tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    if (state.track == null || !event.getAfter().tracks.stream()
        .filter(t -> t.id.equals(state.track.id)).findFirst().isPresent()) {

      // no previous track, so use first one
      try {
        playerThread.openTrack(event.getAfter().tracks.get(0));
      } catch (Exception e) {
        LOG.error("Unable to open track {}",
            event.getAfter().tracks.get(0).filename, e);
      }
      return;
    }

    // if track exists in both playlists and index is <= previous index,
    // then track hasn't been moved, so we can keep playing
    int beforeIndex = findTrack(state.track, event.getBefore().tracks);
    int afterIndex = findTrack(state.track, event.getAfter().tracks);
    if (beforeIndex < 0 || afterIndex < 0 || beforeIndex > afterIndex) {
      playerThread.closeTrack();
      return;
    }
  }

  int findTrack(Track track, List<Track> tracks) {
    for (int i = 0; i < tracks.size(); i++) {
      Track candidate = tracks.get(i);
      if (candidate.id.equals(track.id)) {
        return i;
      }
    }
    return -1;
  }

  void tracksRemoved(PlaylistTracksRemovedEvent event) {
    LOG.debug("Tracks removed");

    NowPlaying state = playerThread.getCurrentState();

    if (event.getAfter().tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    if (state.track == null || !event.getAfter().tracks.stream()
        .filter(t -> t.id.equals(state.track.id)).findFirst().isPresent()) {

      // no previous track, so use first one
      try {
        playerThread.openTrack(event.getAfter().tracks.get(0));
      } catch (Exception e) {
        LOG.error("Unable to open track {}",
            event.getAfter().tracks.get(0).filename, e);
      }
    }
  }

  void trackFinished(Track track) {
    LOG.debug("Track finished");

    var dbState = doorbellState.get();
    var dbVolume = doorbellVolume.get();

    if (dbState != null) {
      // doorbell was playing
      LOG.info("Doorbell finished");
      playerThread.closeTrack();
      if (dbState.track != null) {
        try {
          playerThread.openTrack(dbState.track);
        } catch (Exception e) {
          LOG.error("Unable to open track {}", dbState.track.filename, e);
          return;
        }
        playerThread.seekPosition(dbState.positionMicros);
      }
      if (dbVolume != null) {
        playerThread.setVolume(dbVolume);
      }
      doorbellState.set(null);
      doorbellVolume.set(null);
      return;
    }

    var playlist = playlistState.getPlaylist();

    if (playlist.tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    // attempt to find track in current playlist and get next
    for (int i = 0; i < playlist.tracks.size(); i++) {
      var candidateTrack = playlist.tracks.get(i);
      if (candidateTrack.id.equals(track.id)) {
        // found current track
        if (i < playlist.tracks.size() - 1) {
          Track nextTrack = playlist.tracks.get(i + 1);
          try {
            playerThread.openTrack(nextTrack);
          } catch (Exception e) {
            LOG.error("Unable to open track {}", nextTrack.filename, e);
            return;
          }
          playerThread.startPlayback();
          return;
        }
        break;
      }
    }

    // didn't find any more tracks, so cue up the first track in the list
    try {
      playerThread.openTrack(playlist.tracks.get(0));
    } catch (Exception e) {
      LOG.error("Unable to open track {}", playlist.tracks.get(0).filename, e);
    }
  }

  @Override public NowPlaying getCurrentState() {
    NowPlaying db = doorbellState.get();
    return (db == null ? playerThread.getCurrentState() : db);
  }

  @Override public void playDoorbellTrack(Track track, double volume) {
    LOG.debug("DOORBELL");
    if (doorbellState.get() != null) {
      LOG.info("Doorbell already playing");
      return;
    }

    playerThread.pausePlayback();
    doorbellState.set(playerThread.getCurrentState());
    doorbellVolume.set(playerThread.getVolume());

    try {
      LOG.info("Playing doorbell track...");
      playerThread.closeTrack();
      playerThread.openTrack(track);
      playerThread.setVolume(volume);
      playerThread.seekPosition(0L);
      playerThread.startPlayback();
    } catch (Exception e) {
      LOG.error("Unable to open track {}", track, e);
    }

  }

  @Override public void play() {
    LOG.debug("PLAY");

    if (doorbellState.get() != null) {
      return;
    }

    var state = playerThread.getCurrentState();
    var playlist = playlistState.getPlaylist();

    if (state.playing) {
      // already playing, no change
      return;
    }

    if (playlist.tracks.isEmpty()) {
      // no tracks available to play
      playerThread.closeTrack();
      return;
    }

    if (state.track == null) {
      // no previous track playing
      try {
        playerThread.openTrack(playlist.tracks.get(0));
      } catch (Exception e) {
        LOG.error("Unable to open track {}", playlist.tracks.get(0).filename,
            e);
        return;
      }
      playerThread.startPlayback();

      return;
    }

    var oTrack =
        playlist.tracks.stream().filter(t -> t.id.equals(state.track.id))
            .findFirst();

    if (oTrack.isPresent()) {
      // previous track is still in playlist, so just unpause
      playerThread.startPlayback();
      return;
    }

    // prevous track not available, so play first one
    try {
      playerThread.openTrack(playlist.tracks.get(0));
    } catch (Exception e) {
      LOG.error("Unable to open track {}", playlist.tracks.get(0).filename, e);
      return;
    }
    playerThread.startPlayback();
  }

  @Override public void stop() {
    LOG.debug("STOP");

    if (doorbellState.get() != null) {
      playerThread.pausePlayback();
      Track t = playerThread.currentTrack;
      if (t != null) {
        trackFinished(t);
      }
      return;
    }

    var state = playerThread.getCurrentState();
    if (!state.playing) {
      return;
    }

    playerThread.pausePlayback();
  }

  @Override public void seekPrev() {
    LOG.debug("PREV");

    if (doorbellState.get() != null) {
      return;
    }

    var state = playerThread.getCurrentState();
    var playlist = playlistState.getPlaylist();

    if (playlist.tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    if (state.track == null) {
      // no previous track, so use first one
      try {
        playerThread.openTrack(playlist.tracks.get(0));
      } catch (Exception e) {
        LOG.error("Unable to open track {}", playlist.tracks.get(0).filename,
            e);
        return;
      }
      if (state.playing) {
        playerThread.startPlayback();
      }
      return;
    }

    // if the current track is more than 15 seconds in, then start it over
    // instead of skipping back
    if (state.track != null && state.positionMicros > 15_000_000L) {
      // start at beginning of track
      playerThread.seekPosition(0L);
      return;
    }

    // attempt to find track in current playlist and get previous
    for (int i = 0; i < playlist.tracks.size(); i++) {
      var track = playlist.tracks.get(i);
      if (track.id.equals(state.track.id)) {
        // found current track
        if (i > 0) {
          Track nextTrack = playlist.tracks.get(i - 1);
          try {
            playerThread.openTrack(nextTrack);
          } catch (Exception e) {
            LOG.error("Unable to open track {}", nextTrack.filename, e);
            return;
          }
          if (state.playing) {
            playerThread.startPlayback();
          }
        }
        return;
      }
    }

    // didn't find track, so stop
    playerThread.closeTrack();
  }

  @Override public void forward30() {
    LOG.debug("FORWARD 30");

    if (doorbellState.get() != null) {
      return;
    }

    var state = playerThread.getCurrentState();
    if (state.track == null) {
      return;
    }

    long len = state.track.lengthMicros;
    long pos = state.positionMicros + 30_000_000L;
    if (pos >= len) {
      pos = len - 1;
    }
    if (pos < 0) {
      pos = 0;
    }

    if (pos != state.positionMicros) {
      playerThread.seekPosition(pos);
    }
  }

  @Override public void back30() {
    LOG.debug("BACK 30");

    if (doorbellState.get() != null) {
      return;
    }

    var state = playerThread.getCurrentState();
    if (state.track == null) {
      return;
    }

    long len = state.track.lengthMicros;
    long pos = state.positionMicros - 30_000_000L;
    if (pos >= len) {
      pos = len - 1;
    }
    if (pos < 0) {
      pos = 0;
    }

    if (pos != state.positionMicros) {
      playerThread.seekPosition(pos);
    }
  }

  @Override public void seekNext() {
    LOG.debug("NEXT");

    if (doorbellState.get() != null) {
      return;
    }

    var state = playerThread.getCurrentState();
    var playlist = playlistState.getPlaylist();

    if (playlist.tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    if (state.track == null) {
      // no previous track, so use first one
      try {
        playerThread.openTrack(playlist.tracks.get(0));
      } catch (Exception e) {
        LOG.error("Unable to open track {}", playlist.tracks.get(0).filename,
            e);
        return;
      }
      if (state.playing) {
        playerThread.startPlayback();
      }
      return;
    }

    // attempt to find track in current playlist and get next
    for (int i = 0; i < playlist.tracks.size(); i++) {
      var track = playlist.tracks.get(i);
      if (track.id.equals(state.track.id)) {
        // found current track
        if (i < playlist.tracks.size() - 1) {
          Track nextTrack = playlist.tracks.get(i + 1);
          try {
            playerThread.openTrack(nextTrack);
          } catch (Exception e) {
            LOG.error("Unable to open track {}", nextTrack.filename, e);
            return;
          }
          if (state.playing) {
            playerThread.startPlayback();
          }
        }
        return;
      }
    }

    // didn't find track, so stop
    playerThread.closeTrack();
  }

  @Override public void seek(Track track) {
    LOG.debug("SEEK {}", track.id);

    if (doorbellState.get() != null) {
      return;
    }

    var playlist = playlistState.getPlaylist();

    if (playlist.tracks.isEmpty()) {
      playerThread.closeTrack();
      return;
    }

    var oTrack =
        playlist.tracks.stream().filter(t -> t.id.equals(track.id)).findFirst();

    if (oTrack.isPresent()) {
      // track is present, so start it
      Track nextTrack = oTrack.get();
      try {
        playerThread.openTrack(nextTrack);
      } catch (Exception e) {
        LOG.error("Unable to open track {}", nextTrack.filename, e);
        return;
      }
      playerThread.startPlayback();
    }
  }

  class MidiPlayerThread extends Thread implements Receiver {

    final CountDownLatch init = new CountDownLatch(1);
    final VelocityMapper velocityMapper;

    volatile boolean shutdown = false;

    Receiver receiver;
    Track currentTrack;
    Sequencer sequencer;
    Transmitter transmitter;
    boolean playing = false;

    MidiPlayerThread(VelocityMapper velocityMapper) {

      setDaemon(true);
      setName("midi-player");
      this.velocityMapper = velocityMapper;
    }

    public void init() throws Exception {
      init.await();
    }

    @Override public void run() {
      Thread hook = null;

      try {
        LOG.debug("Opening MIDI device...");
        midiDevice.open();
        LOG.debug("MIDI device opened.");

        LOG.debug("Opening MIDI receiver...");
        receiver = midiDevice.getReceiver();
        LOG.debug("MIDI receiver opened.");

        hook = new Thread(() -> reset(receiver));
        Runtime.getRuntime().addShutdownHook(hook);

        // all done
        init.countDown();

        while (!shutdown) {
          try {
            eventLoop();
          } catch (Exception e) {
            LOG.error("Exception in MIDI player", e);
          }
        }
      } catch (Exception e) {
        LOG.error("Fatal exception in MIDI player", e);
        System.exit(1);
      } finally {
        if (currentTrack != null) {
          closeTrack();
        }
        if (receiver != null) {
          LOG.debug("Resetting MIDI receiver...");
          reset(receiver);
          LOG.debug("Reset MIDI receiver.");

          if (hook != null) {
            Runtime.getRuntime().removeShutdownHook(hook);
          }
          LOG.debug("Closing MIDI receiver...");
          receiver.close();
          LOG.debug("MIDI receiver closed.");
        }

        LOG.debug("Closing MIDI device...");
        midiDevice.close();
        LOG.debug("MIDI device closed.");
      }
    }

    synchronized void openTrack(Track track) throws Exception {
      closeTrack();

      sequencer = MidiSystem.getSequencer(false);
      sequencer.open();

      boolean readTrack = false;
      try (InputStream in = new BufferedInputStream(
          Files.newInputStream(track.filename))) {
        sequencer.setSequence(in);
        readTrack = true;
      } finally {
        if (!readTrack) {
          sequencer.close();
          sequencer = null;
        }
      }
      transmitter = sequencer.getTransmitter();
      transmitter.setReceiver(this);
      currentTrack = track;
    }

    synchronized void closeTrack() {
      boolean needPause = playing;
      playing = false;
      if (sequencer != null && sequencer.isOpen()) {
        sequencer.stop();
      }
      reset(receiver);
      transmitter = null;
      if (sequencer != null) {
        sequencer.close();
        sequencer = null;
      }
      currentTrack = null;
      if (needPause) {
        try {
          Thread.sleep(500L);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    }

    synchronized void startPlayback() {
      if (sequencer == null || sequencer.isRunning()) {
        return;
      }
      reset(receiver);
      sequencer.start();
      playing = true;
    }

    synchronized void pausePlayback() {
      if (sequencer == null || (!sequencer.isRunning() && !playing)) {
        return;
      }

      sequencer.stop();
      playing = false;
      reset(receiver);
    }

    synchronized void seekPosition(long position) {
      if (sequencer == null || position > sequencer.getMicrosecondLength()) {
        return;
      }
      if (sequencer.isRunning()) {
        sequencer.stop();
        reset(receiver);
        sequencer.setMicrosecondPosition(position);
        sequencer.start();
      } else {
        sequencer.setMicrosecondPosition(position);
      }
    }

    void setVolume(double value) {
      velocityMapper.setVolume(value);
    }

    double getVolume() {
      return velocityMapper.getVolume();
    }

    synchronized NowPlaying getCurrentState() {
      NowPlaying np = new NowPlaying();
      if (currentTrack != null) {
        np.track = currentTrack;
      }
      if (sequencer != null) {
        np.positionMicros = sequencer.getMicrosecondPosition();
        if (sequencer.isRunning() && playing) {
          np.playing = true;
        }
      }
      return np;
    }

    synchronized void reset(Receiver rx) {
      try {
        // all notes off
        rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 123, 0), -1L);

        // damper off
        rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 64, 0), -1L);

        // portamento off
        rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 65, 0), -1L);

        // sustenuto off
        rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 66, 0), -1L);
      } catch (InvalidMidiDataException e) {
        throw new RuntimeException(e);
      }
    }

    @Override public void send(MidiMessage message, long timeStamp) {
      if (message instanceof SysexMessage) {
        return;
      }

      if (!(message instanceof ShortMessage)) {
        receiver.send(message, timeStamp);
        return;
      }

      ShortMessage sm = (ShortMessage) message;

      int data1 = sm.getData1();
      int data2 = sm.getData2();
      int cmd = sm.getCommand();

      LOG.debug("Incoming message: {}, {}, {}", cmd, data1, data2);

      switch (cmd) {
      case ShortMessage.NOTE_ON:
        if (data2 == 0) { // note off
          cmd = ShortMessage.NOTE_OFF;
          data2 = 0;
          LOG.trace("NOTE OFF: {} {}", data1, data2);
        } else {
          data2 = velocityMapper.map(data1, data2);
          LOG.trace("NOTE ON: {} {}", data1, data2);
        }
        break;
      case ShortMessage.NOTE_OFF:
        data2 = 0;
        LOG.trace("NOTE OFF: {} {}", data1, data2);
        break;
      }

      try {
        sm.setMessage(cmd, 0, data1, data2);
      } catch (InvalidMidiDataException e) {
        LOG.warn("Generated invalid MIDI message", e);
      }

      receiver.send(sm, -1);
    }

    @Override public void close() {
      // do nothing
    }

    Track finishedTrack() throws Exception {
      Track result = null;
      synchronized (this) {
        LOG.trace(
            "Checking for finished track. Playing: {}, track: {}, open: {}, running: {}",
            playing, currentTrack, sequencer != null && sequencer.isOpen(),
            sequencer != null && sequencer.isRunning());

        if (playing && currentTrack != null && sequencer != null && sequencer
            .isOpen() && !sequencer.isRunning()) {
          result = currentTrack;
          closeTrack();
        }
      }
      return result;
    }

    void eventLoop() throws Exception {

      // detect track playback finished
      Track finishedTrack = finishedTrack();
      if (finishedTrack != null) {
        trackFinished(finishedTrack);
      }

      Thread.sleep(100L);
    }

    void shutdown() throws Exception {
      shutdown = true;
      synchronized (this) {
        notifyAll();
        wait(30000L);
      }
    }
  }

}
