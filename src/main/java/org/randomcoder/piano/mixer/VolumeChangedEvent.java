package org.randomcoder.piano.mixer;

public class VolumeChangedEvent {

  private final double before;
  private final double after;

  public VolumeChangedEvent(double before, double after) {
    this.before = before;
    this.after = after;
  }

  public double getBefore() {
    return before;
  }

  public double getAfter() {
    return after;
  }

}
