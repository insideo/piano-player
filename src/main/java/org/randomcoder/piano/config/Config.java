package org.randomcoder.piano.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import javax.sound.midi.MidiDevice;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;

@JsonIgnoreProperties(ignoreUnknown = false) public class Config {

  public String bindAddress = "*";
  public int bindPort = 8080;
  public int securePort = 443;
  public boolean forward = false;
  public boolean forceHttps = false;
  public boolean sendSts = false;
  public long stsMaxAge = 1;
  public int defaultMinKeyVelocity = 0;
  public int maxKeyVelocity = 100;
  public int velocityPivotPoint = 32;
  public int velocityLowRange = 5;
  public int keyCalibrationOffset = 0;

  @JsonProperty("libraryPath") private String libraryPathString;

  @JsonIgnore public Path libraryPath;

  @JsonProperty("doorbellMidi") private String doorbellMidiString;

  @JsonIgnore public Path doorbellMidi;

  @JsonProperty("libraryMetadataCache") private String
      libraryMetadataCacheString;

  @JsonIgnore public Path libraryMetadataCache;

  @JsonProperty("keyCalibrationFile") private String keyCalibrationFile;

  @JsonIgnore public int[] minKeyVelocities;

  @JsonProperty("julLogLevel") private String julLogLevelString = "INFO";

  @JsonIgnore public java.util.logging.Level julLogLevel;

  public int threadPoolMin = 10;
  public int threadPoolMax = 200;
  public int threadPoolIdleTimeoutMs = 60000;

  public double volumeMin = 0.0;
  public double volumeMax = 1.0;
  public double volumeDefault = 0.5;
  public double doorbellVolume = 0.5;

  @JsonProperty("midiDevice") private MidiDeviceDescriptor midiDeviceDescriptor;

  @JsonIgnore public MidiDevice midiDevice;

  private void init() throws IOException {
    Objects.requireNonNull(libraryPathString, "libraryPath is required");
    Objects.requireNonNull(midiDeviceDescriptor, "midiDevice is required");

    {
      Path p = Paths.get(libraryPathString).toAbsolutePath();
      if (!Files.isDirectory(p)) {
        throw new IllegalArgumentException(
            String.format("libraryDir %s is invalid", p));
      }
      libraryPath = p;
    }

    if (doorbellMidiString != null) {
      Path p = Paths.get(doorbellMidiString);
      if (!Files.isRegularFile(p)) {
        throw new IllegalArgumentException(
            String.format("doorbellMidi %s is invalid", p));
      }
      doorbellMidi = p;
    }

    if (libraryMetadataCacheString != null) {
      Path p = Paths.get(libraryMetadataCacheString).toAbsolutePath();
      libraryMetadataCache = p;
    }

    if (keyCalibrationFile != null) {
      Path p = Paths.get(keyCalibrationFile).toAbsolutePath();
      if (!Files.isRegularFile(p)) {
        throw new IllegalArgumentException(
            String.format("keyCalibrationFile %s is invalid", p));
      }
      keyCalibrationFile = p.toString();
      minKeyVelocities =
          parseMinKeyVelocities(p, defaultMinKeyVelocity, keyCalibrationOffset);
    } else {
      minKeyVelocities = new int[128];
      for (int i = 0; i < minKeyVelocities.length; i++) {
        minKeyVelocities[i] = defaultMinKeyVelocity + keyCalibrationOffset;
      }
    }

    for (int i = 0; i < minKeyVelocities.length; i++) {
      minKeyVelocities[i] = Math.max(minKeyVelocities[i], 1);
      minKeyVelocities[i] = Math.min(minKeyVelocities[i], 127);
    }

    midiDevice = midiDeviceDescriptor.create();

    julLogLevel = java.util.logging.Level.parse(julLogLevelString);
  }

  private static int[] parseMinKeyVelocities(Path path, int defaultValue,
      int keyCalibrationOffset) throws IOException {
    var config = new ObjectMapper(new YAMLFactory())
        .readValue(path.toUri().toURL(), CalibrationData.class);
    var map = config.getKeyCalibrationsMap();
    int[] result = new int[128];
    for (int i = 0; i < result.length; i++) {
      result[i] = Optional.ofNullable(map.get(Integer.valueOf(i)))
          .map(e -> e.minVelocity).orElse(defaultValue) + keyCalibrationOffset;
    }
    return result;
  }

  public static Config parse(URL url) throws IOException {
    var config =
        new ObjectMapper(new YAMLFactory()).readValue(url, Config.class);
    config.init();

    return config;
  }

  @Override public String toString() {
    return String.format(
        "%n" + "  bindAddress: %s%n" + "  bindPort: %d%n" + "  securePort: %d%n"
            + "  forward: %s%n" + "  forceHttps: %s%n" + "  sendSts: %s%n"
            + "  stsMaxAge: %d%n" + "  libraryPath: %s%n"
            + "  libraryMetadataCache: %s%n" + "  doorbellMidi: %s%n"
            + "  doorbellVolume: %f%n" + "  julLogLevel: %s%n"
            + "  threadPoolMin: %d%n" + "  threadPoolMax: %d%n"
            + "  threadPoolIdleTimeoutMs: %d%n" + "  volumeMin: %f%n"
            + "  volumeMax: %f%n" + "  volumeDefault: %f%n"
            + "  maxKeyVelocity: %d%n" + "  velocityPivotPoint: %d%n"
            + "  velocityLowRange: %d%n" + "  defaultMinKeyVelocity: %d%n"
            + "  keyCalibrationFile: %s%n" + "  keyCalibrationOffset: %d%n"
            + "  midiDevice: { name=%s, vendor=%s, version=%s, desc=%s }%n",
        bindAddress, bindPort, securePort, forward, forceHttps, sendSts,
        stsMaxAge, libraryPath, libraryMetadataCache, doorbellMidi,
        doorbellVolume, julLogLevel, threadPoolMin, threadPoolMax,
        threadPoolIdleTimeoutMs, volumeMin, volumeMax, volumeDefault,
        maxKeyVelocity, velocityPivotPoint, velocityLowRange,
        defaultMinKeyVelocity, keyCalibrationFile, keyCalibrationOffset,
        midiDevice.getDeviceInfo().getName(),
        midiDevice.getDeviceInfo().getVendor(),
        midiDevice.getDeviceInfo().getVersion(),
        midiDevice.getDeviceInfo().getDescription());
  }

}
