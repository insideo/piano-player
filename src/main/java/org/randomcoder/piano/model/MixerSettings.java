package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class MixerSettings {

  public double volume;

  public MixerSettings() {

  }

  public MixerSettings(double volume) {
    this.volume = volume;
  }

  @JsonInclude(value = Include.NON_EMPTY) public Map<String, URI> links =
      new HashMap<>();

  public MixerSettings withLinks(UriInfo info) {
    MixerSettings clone = new MixerSettings(volume);
    clone.links.put("self", info.getBaseUriBuilder().path("/mixer").build());
    return clone;
  }

}
