package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.NowPlaying;
import org.randomcoder.piano.player.Player;
import org.randomcoder.piano.playlist.PlaylistState;

@Path("/now-playing") public class NowPlayingResource {

  @Inject public TrackLibrary library;

  @Inject public PlaylistState playlistState;

  @Inject public Player player;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public NowPlaying getStatus(@Context UriInfo uriInfo) {
    return player.getCurrentState().withLinks(uriInfo);
  }

  @POST @Path("play!") public Response play(@Context UriInfo uriInfo) {
    player.play();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("stop!") public Response stop(@Context UriInfo uriInfo) {
    player.stop();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("next!") public Response seekNext(@Context UriInfo uriInfo) {
    player.seekNext();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("prev!") public Response seekPrev(@Context UriInfo uriInfo) {
    player.seekPrev();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("seek!/{id}")
  public Response seekTrack(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    player.seek(library.fetchTrack(id));
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("forward30!")
  public Response forward30Seconds(@Context UriInfo uriInfo) {
    player.forward30();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

  @POST @Path("back30!")
  public Response back30Seconds(@Context UriInfo uriInfo) {
    player.back30();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/now-playing").build())
        .build();
  }

}
