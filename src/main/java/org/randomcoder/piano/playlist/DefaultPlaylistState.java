package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;
import org.randomcoder.piano.model.Track;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class DefaultPlaylistState implements PlaylistState {

  private final AtomicReference<Playlist> current;
  private final CopyOnWriteArrayList<Consumer<PlaylistClearedEvent>>
      clearedListeners;
  private final CopyOnWriteArrayList<Consumer<PlaylistShuffledEvent>>
      shuffledListeners;
  private final CopyOnWriteArrayList<Consumer<PlaylistTracksAddedEvent>>
      tracksAddedListeners;
  private final CopyOnWriteArrayList<Consumer<PlaylistTracksRemovedEvent>>
      tracksRemovedListeners;

  public DefaultPlaylistState() {
    this.current = new AtomicReference<>(new Playlist());
    this.clearedListeners = new CopyOnWriteArrayList<>();
    this.shuffledListeners = new CopyOnWriteArrayList<>();
    this.tracksAddedListeners = new CopyOnWriteArrayList<>();
    this.tracksRemovedListeners = new CopyOnWriteArrayList<>();
  }

  @Override public void addPlaylistClearedListener(
      Consumer<PlaylistClearedEvent> listener) {
    clearedListeners.add(listener);
  }

  @Override public void addPlaylistShuffledListener(
      Consumer<PlaylistShuffledEvent> listener) {
    shuffledListeners.add(listener);
  }

  @Override public void addPlaylistTracksAddedListener(
      Consumer<PlaylistTracksAddedEvent> listener) {
    tracksAddedListeners.add(listener);
  }

  @Override public void addPlaylistTracksRemovedListener(
      Consumer<PlaylistTracksRemovedEvent> listener) {
    tracksRemovedListeners.add(listener);
  }

  @Override public Playlist getPlaylist() {
    return current.get();
  }

  @Override public void clear() {
    var before = current.get();
    var after = new Playlist();
    var event = new PlaylistClearedEvent(before);

    // notify event listeners
    clearedListeners.stream().forEach(c -> c.accept(event));

    // update internal state
    current.set(after);
  }

  @Override public void shuffle() {
    var before = current.get();
    var after = before.clone();
    var random = new Random();

    int len = after.tracks.size();
    for (int i = 0; i < len; i++) {
      int pos = random.nextInt(len - i);
      if (i == pos) {
        continue;
      }
      Track a = after.tracks.get(i);
      Track b = after.tracks.get(pos);
      after.tracks.set(i, b);
      after.tracks.set(pos, a);
    }

    var event = new PlaylistShuffledEvent(before, after);

    // notify event listeners
    shuffledListeners.stream().forEach(c -> c.accept(event));

    // update internal state
    current.set(after);
  }

  @Override public void add(Collection<Track> tracks) {
    var before = current.get();
    var after = before.clone();
    var added = new ArrayList<>(tracks);

    Set<String> trackIds = new HashSet<>();
    for (Track track : tracks) {
      trackIds.add(track.id);
    }

    after.tracks.removeIf(t -> trackIds.contains(t.id));
    after.tracks.addAll(added);

    var event = new PlaylistTracksAddedEvent(before, after, added);

    // notify event listeners
    tracksAddedListeners.stream().forEach(c -> c.accept(event));

    // update internal state
    current.set(after);
  }

  @Override public void remove(Collection<Track> tracks) {
    var before = current.get();
    var after = before.clone();
    var removed = new ArrayList<>(tracks);

    Set<String> trackIds = new HashSet<>();
    for (Track track : tracks) {
      trackIds.add(track.id);
    }
    Set<String> existingIds = new HashSet<>();
    for (Track track : before.tracks) {
      existingIds.add(track.id);
    }

    removed.removeIf(t -> !existingIds.contains(t.id));
    after.tracks.removeIf(t -> trackIds.contains(t.id));

    var event = new PlaylistTracksRemovedEvent(before, after, removed);

    // notify event listeners
    tracksRemovedListeners.stream().forEach(c -> c.accept(event));

    // update internal state
    current.set(after);
  }

}
