package org.randomcoder.piano.model;

import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.Map;

public class Library {

  public Map<String, URI> links;

  public Library(UriInfo uriInfo) {
    this.links = Map.of("category",
        uriInfo.getBaseUriBuilder().path("/library/category").build(),
        "composer",
        uriInfo.getBaseUriBuilder().path("/library/composer").build(),
        "performer",
        uriInfo.getBaseUriBuilder().path("/library/performer").build(),
        "refresh",
        uriInfo.getBaseUriBuilder().path("/library/refresh!").build());
  }

}
