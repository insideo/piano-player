package org.randomcoder.piano.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import org.randomcoder.piano.PianoPlayer;
import org.randomcoder.piano.config.CalibrationData;
import org.randomcoder.piano.config.KeyCalibration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class CalibratePiano {
  private static final int NOTE_MIN = 23;
  private static final int NOTE_MAX = 105;

  private static final Logger LOG =
      LoggerFactory.getLogger(CalibratePiano.class);

  private final MidiDevice device;

  public enum CalibrationPhase {
    GENERATE, UPDATE, VALIDATE
  }

  public CalibratePiano(MidiDevice device) {
    this.device = device;
  }

  public static void usage() {
    System.out.printf(
        "Usage: %s <config-file> calibrate <yaml-file> <phase> <offset> <increment> <duration> <pause> <count>%n",
        PianoPlayer.class.getCanonicalName());
    System.out.printf("         Calibrates piano key MIDI response.%n%n");
    System.out.printf(
        "         This will adjust minimum viable velocity for each key.%n%n");
    System.out.printf(
        "        yaml-file   YAML file to maintain state in (backups will be made)%n");
    System.out.printf("            phase   One of:%n");
    System.out.printf(
        "                       generate - initial calibration of global minimum%n");
    System.out
        .printf("                       update - adjusts individual keys%n");
    System.out
        .printf("                       validate - validates key response%n");
    System.out.printf(
        "           offset   Number of velocity steps to adjust around existing range%n");
    System.out.printf(
        "                    (also used as start for 'generate' phase)%n");
    System.out.printf(
        "        increment   Number of velocity steps to increment for each test%n");
    System.out.printf("         duration   Milliseconds to hold each note%n");
    System.out
        .printf("            pause   Milliseconds to wait between notest%n");
    System.out
        .printf("            count   Number of times to play each note%n");

    System.out.flush();
  }

  public void run(List<String> args) throws Exception {
    if (args.size() != 7) {
      usage();
      return;
    }

    Path yamlFile = Paths.get(args.get(0)).toAbsolutePath();
    CalibrationPhase phase =
        CalibrationPhase.valueOf(args.get(1).toUpperCase(Locale.US));
    int offset = Integer.parseInt(args.get(2));
    int increment = Integer.parseInt(args.get(3));
    int duration = Integer.parseInt(args.get(4));
    int pause = Integer.parseInt(args.get(5));
    int count = Integer.parseInt(args.get(6));

    LOG.info("Configuration:");
    LOG.info("    yaml-file: {}", yamlFile);
    LOG.info("        phase: {}", phase);
    LOG.info("       offset: {}", offset);
    LOG.info("    increment: {}", increment);
    LOG.info("     duration: {}", duration);
    LOG.info("        pause: {}", pause);
    LOG.info("        count: {}", count);

    try (var reader = new BufferedReader(
        Optional.ofNullable(System.console()).map(c -> c.reader()).orElse(
            new InputStreamReader(System.in, StandardCharsets.UTF_8)))) {
      try {
        try (var rx = device.getReceiver()) {
          device.open();
          Thread hook = null;
          try {
            hook = new Thread(() -> reset(rx));
            Runtime.getRuntime().addShutdownHook(hook);

            switch (phase) {
            case GENERATE:
              generate(yamlFile, offset, increment, duration, pause, count,
                  reader, rx);
              break;
            case UPDATE:
              update(yamlFile, offset, increment, duration, pause, count,
                  reader, rx);
              break;
            case VALIDATE:
              validate(yamlFile, offset, increment, duration, pause, count,
                  reader, rx);
              break;
            }
          } finally {
            reset(rx);
            if (hook != null) {
              Runtime.getRuntime().removeShutdownHook(hook);
            }
          }
        }
      } finally {
        device.close();
      }
    }

  }

  private char prompt(BufferedReader reader, String allowed, String prompt)
      throws IOException {
    allowed = allowed.toUpperCase(Locale.US);
    while (true) {
      System.out.print(prompt);
      System.out.print(": ");
      System.out.flush();
      String result = reader.readLine().trim().toUpperCase(Locale.US);
      if (result.length() > 1) {
        continue;
      } else if (result.length() < 1) {
        if (allowed.contains("-")) {
          return '-';
        }
        continue;
      }
      if (allowed.contains(result)) {
        return result.charAt(0);
      }
    }
  }

  private CalibrationData loadData(Path yaml) throws IOException {
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    try (var in = Files.newInputStream(yaml)) {
      return mapper.readValue(in, CalibrationData.class);
    }
  }

  private void saveData(Path yaml, CalibrationData cal) throws IOException {
    backupFiles(yaml);

    YAMLFactory yamlFactory = new YAMLFactory()
        .configure(YAMLGenerator.Feature.WRITE_DOC_START_MARKER, false)
        .configure(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID, false)
        .configure(YAMLGenerator.Feature.MINIMIZE_QUOTES, true)
        .configure(YAMLGenerator.Feature.ALWAYS_QUOTE_NUMBERS_AS_STRINGS, false)
        .configure(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID, false);

    ObjectMapper mapper = new ObjectMapper(yamlFactory);
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);

    try (var out = Files.newOutputStream(yaml)) {
      mapper.writeValue(out, cal);
    }
  }

  public void generate(Path yaml, int offset, int increment, int duration,
      int pause, int count, BufferedReader reader, Receiver rx)
      throws Exception {

    System.out.println();
    System.out.println("Initial data generation -- phase 1");
    System.out.println();
    System.out.println(
        "A full keyboard scale will be played from bottom to top in increasing");
    System.out.println(
        "velocities. After each increment, you will be asked to confirm");
    System.out.println(
        "whether any notes were clearly heard. This will be used to establish");
    System.out.println("a baseline for testing.");
    System.out.println();
    System.out.println("Press [ENTER] to begin...");
    System.out.flush();

    reader.readLine();

    if (offset < 0) {
      offset = 0;
    }
    if (offset > 127) {
      offset = 127;
    }

    if (duration < 10) {
      duration = 10;
    }
    if (pause < 0) {
      pause = 0;
    }
    if (count < 1) {
      count = 1;
    }
    int vel;
    for (vel = offset; vel < 128; vel += increment) {
      System.out.printf("Testing velocity of %d...%n", vel);
      System.out.flush();
      for (int note = NOTE_MIN; note <= NOTE_MAX; note++) {
        for (int n = 0; n < count; n++) {
          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, note, vel), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, note, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }
        }
      }

      char response = prompt(reader, "RNPG",
          String.format("Velocity %3d: [R]etry, [N]ext, [P]rev, [G]ood", vel));

      if ('R' == response) {
        vel -= increment;
        continue;
      } else if ('N' == response) {
        continue;
      } else if ('P' == response) {
        vel -= (increment * 2);
        if (vel <= -increment) {
          vel = -increment + 1;
        }
        continue;
      } else if ('G' == response) {
        break;
      }
    }
    if (vel > 127) {
      System.out.println("Error: No starting velocity found, aborting.");
      return;
    }

    Map<Integer, KeyCalibration> keyData = new TreeMap<>();
    for (int i = NOTE_MIN; i <= NOTE_MAX; i++) {
      keyData.put(Integer.valueOf(i), new KeyCalibration(i, vel));
    }

    CalibrationData cal = new CalibrationData(keyData);

    saveData(yaml, cal);
  }

  public void update(Path yaml, int offset, int increment, int duration,
      int pause, int count, BufferedReader reader, Receiver rx)
      throws Exception {

    System.out.println();
    System.out.println("Updated data generation (phase 2)");
    System.out.println();
    System.out.println(
        "Each note on the keyboard will be played at various velocities.");
    System.out.println(
        "After each note, you will be asked to confirm whether each note");
    System.out.println("was clearly voiced.");
    System.out.println();
    System.out.println("Press [ENTER] to begin...");
    System.out.flush();

    reader.readLine();

    CalibrationData cal = loadData(yaml);

    if (offset < 0) {
      offset = 0;
    }
    if (offset > 127) {
      offset = 127;
    }
    if (increment < 1) {
      increment = 1;
    }
    if (increment > 127) {
      increment = 127;
    }

    if (duration < 10) {
      duration = 10;
    }
    if (pause < 0) {
      pause = 0;
    }
    if (count < 1) {
      count = 1;
    }

    var map = cal.getKeyCalibrationsMap();

    for (int note = NOTE_MIN; note <= NOTE_MAX; note++) {
      KeyCalibration keyData = map.get(Integer.valueOf(note));
      if (keyData == null) {
        System.out
            .printf("Skipping note %d as no calibration data was found.%n",
                note);
        System.out.flush();
        continue;
      }

      int startVelocity = keyData.minVelocity - offset;
      if (startVelocity < 1) {
        startVelocity = 1;
      }
      int vel;
      boolean skip = false;
      for (vel = startVelocity; vel < 128; vel += increment) {
        System.out.printf("Note %d, velocity %d: ", note, vel);
        System.out.flush();
        for (int n = 0; n < count; n++) {
          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, note, vel), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, note, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }
        }

        char response = prompt(reader, "RNPGS-",
            String.format("[R]etry, [N]ext(*), [P]rev, [G]ood, [S]kip"));

        if ('R' == response) {
          vel -= increment;
          continue;
        } else if ('N' == response || '-' == response) {
          continue;
        } else if ('P' == response) {
          vel -= (increment * 2);
          if (vel <= -increment) {
            vel = -increment + 1;
          }
          continue;
        } else if ('G' == response) {
          break;
        } else if ('S' == response) {
          skip = true;
          break;
        }
      }
      if (vel <= 127 && !skip) {
        keyData.minVelocity = vel;
      }
    }

    System.out.println("Calibration complete. Writing updated values...");
    System.out.flush();

    cal = new CalibrationData(map);

    saveData(yaml, cal);
  }

  public void validate(Path yaml, int offset, int increment, int duration,
      int pause, int count, BufferedReader reader, Receiver rx)
      throws Exception {

    System.out.println();
    System.out.println("Verifying calibration (phase 3)");
    System.out.println();
    System.out.println("Press [ENTER] to begin...");
    System.out.flush();
    reader.readLine();

    if (offset < 0) {
      offset = 0;
    }
    if (offset > 127) {
      offset = 127;
    }
    if (increment < 1) {
      increment = 1;
    }
    if (increment > 127) {
      increment = 127;
    }

    if (duration < 10) {
      duration = 10;
    }
    if (pause < 0) {
      pause = 0;
    }
    if (count < 1) {
      count = 1;
    }

    CalibrationData cal = loadData(yaml);
    var map = cal.getKeyCalibrationsMap();
    for (int i = NOTE_MIN; i <= NOTE_MAX; i++) {
      if (map.get(Integer.valueOf(i)) == null) {
        System.out.printf("Missing data for note %d, aborting.%n", i);
        return;
      }
    }

    System.out.println("Test 1: Arpeggiated major chords at min velocity");
    System.out.println();

    for (int i = 0; i < count; i++) {
      for (int note = NOTE_MIN; note <= (NOTE_MIN + 11); note++) {
        int n1 = note;
        int n2 = note + 4;
        int n3 = note + 7;

        do {
          int v1 = map.get(Integer.valueOf(n1)).minVelocity;
          int v2 = map.get(Integer.valueOf(n2)).minVelocity;
          int v3 = map.get(Integer.valueOf(n3)).minVelocity;

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n1, v1), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n1, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n2, v2), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n2, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n3, v3), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n3, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          n1 += 12;
          n2 += 12;
          n3 += 12;
        } while (n3 <= NOTE_MAX);
      }
    }

    System.out
        .println("Test 2: Arpeggiated major chords at min velocity + 25%");
    System.out.println();

    for (int i = 0; i < count; i++) {
      for (int note = NOTE_MIN; note <= (NOTE_MIN + 11); note++) {
        int n1 = note;
        int n2 = note + 4;
        int n3 = note + 7;

        do {
          int v1 = map.get(Integer.valueOf(n1)).minVelocity;
          int v2 = map.get(Integer.valueOf(n2)).minVelocity;
          int v3 = map.get(Integer.valueOf(n3)).minVelocity;

          v1 = Math.min(v1 + ((int) (((double) (128 - v1)) * .25d)), 127);
          v2 = Math.min(v2 + ((int) (((double) (128 - v2)) * .25d)), 127);
          v3 = Math.min(v3 + ((int) (((double) (128 - v3)) * .25d)), 127);

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n1, v1), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n1, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n2, v2), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n2, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n3, v3), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n3, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          n1 += 12;
          n2 += 12;
          n3 += 12;
        } while (n3 <= NOTE_MAX);
      }
    }

    System.out
        .println("Test 3: Arpeggiated major chords at min velocity + 50%");
    System.out.println();

    for (int i = 0; i < count; i++) {
      for (int note = NOTE_MIN; note <= (NOTE_MIN + 11); note++) {
        int n1 = note;
        int n2 = note + 4;
        int n3 = note + 7;

        do {
          int v1 = map.get(Integer.valueOf(n1)).minVelocity;
          int v2 = map.get(Integer.valueOf(n2)).minVelocity;
          int v3 = map.get(Integer.valueOf(n3)).minVelocity;

          v1 = Math.min(v1 + ((int) (((double) (128 - v1)) * .5d)), 127);
          v2 = Math.min(v2 + ((int) (((double) (128 - v2)) * .5d)), 127);
          v3 = Math.min(v3 + ((int) (((double) (128 - v3)) * .5d)), 127);

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n1, v1), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n1, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n2, v2), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n2, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          rx.send(new ShortMessage(ShortMessage.NOTE_ON, 0, n3, v3), -1L);
          Thread.sleep(duration);
          rx.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, n3, 0), -1L);
          if (pause > 0) {
            Thread.sleep(pause);
          }

          n1 += 12;
          n2 += 12;
          n3 += 12;
        } while (n3 <= NOTE_MAX);
      }
    }

  }

  static void backupFiles(Path yaml) throws IOException {
    for (int i = 8; i >= 0; i--) {
      Path current = (i == 0) ? yaml : yaml.getParent()
          .resolve(String.format("%s.%d", yaml.getFileName(), i));
      Path backup = yaml.getParent()
          .resolve(String.format("%s.%d", yaml.getFileName(), i + 1));

      if (Files.exists(current)) {
        Files.move(current, backup, StandardCopyOption.REPLACE_EXISTING);
      }
    }
  }

  static void reset(Receiver rx) {
    try {
      // all notes off
      rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 123, 0), -1L);

      // damper off
      rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 64, 0), -1L);

      // portamento off
      rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 65, 0), -1L);

      // sustenuto off
      rx.send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 66, 0), -1L);
    } catch (InvalidMidiDataException e) {
      throw new RuntimeException(e);
    }
  }

}
