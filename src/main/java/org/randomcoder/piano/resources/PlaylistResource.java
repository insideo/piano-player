package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.model.Playlist;
import org.randomcoder.piano.playlist.PlaylistState;

@Path("/playlist") public class PlaylistResource {

  @Inject public PlaylistState playlistState;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Playlist getPlaylist(@Context UriInfo uriInfo) {
    return playlistState.getPlaylist().withLinks(uriInfo);
  }

  @POST @Path("shuffle!")
  public Response shufflePlaylist(@Context UriInfo uriInfo) {
    playlistState.shuffle();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/playlist").build())
        .build();
  }

  @DELETE public Response clearPlaylist(@Context UriInfo uriInfo) {
    playlistState.clear();
    return Response.noContent()
        .location(uriInfo.getBaseUriBuilder().path("/playlist").build())
        .build();
  }

}
