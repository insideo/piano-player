package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(value = Include.ALWAYS) public class NowPlaying {

  public Track track;
  public long positionMicros;
  public boolean playing;

  @JsonInclude(value = Include.NON_EMPTY) public Map<String, URI> links =
      new HashMap<>();

  public NowPlaying() {
  }

  public NowPlaying(Track track, long positionMicros, boolean playing) {
    this.track = track;
    this.positionMicros = positionMicros;
    this.playing = playing;
  }

  public NowPlaying withLinks(UriInfo info) {
    NowPlaying clone = new NowPlaying(track, positionMicros, playing);
    if (track != null) {
      clone.track = track.withLinks(info, TrackLinkContext.NOW_PLAYING);
    }
    clone.links
        .put("self", info.getBaseUriBuilder().path("/now-playing").build());
    clone.links.put("play",
        info.getBaseUriBuilder().path("/now-playing/play!").build());
    clone.links.put("stop",
        info.getBaseUriBuilder().path("/now-playing/stop!").build());
    clone.links.put("prev",
        info.getBaseUriBuilder().path("/now-playing/prev!").build());
    clone.links.put("next",
        info.getBaseUriBuilder().path("/now-playing/next!").build());
    clone.links.put("seek",
        info.getBaseUriBuilder().path("/now-playing/seek!").build());
    clone.links.put("forward30",
        info.getBaseUriBuilder().path("/now-playing/forward30!").build());
    clone.links.put("back30",
        info.getBaseUriBuilder().path("/now-playing/back30!").build());
    return clone;
  }

}
