package org.randomcoder.piano.model;

public enum TrackLinkContext {
  LIBRARY, NOW_PLAYING;
}
