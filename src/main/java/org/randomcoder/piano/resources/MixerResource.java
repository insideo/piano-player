package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.mixer.Mixer;
import org.randomcoder.piano.model.MixerSettings;

@Path("/mixer") public class MixerResource {

  @Inject public Mixer mixer;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public MixerSettings getMixerSettings(@Context UriInfo uriInfo) {
    return new MixerSettings(mixer.getVolume()).withLinks(uriInfo);
  }

  @PUT @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Response putMixerSettings(MixerSettings settings,
      @Context UriInfo uriInfo) {
    mixer.setVolume(settings.volume);

    return Response.created(uriInfo.getBaseUriBuilder().path("/mixer").build())
        .build();
  }

}
