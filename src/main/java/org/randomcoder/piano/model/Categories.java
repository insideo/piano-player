package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(Include.NON_NULL) public class Categories {

  public List<Category> categories;

  public Categories(List<Category> categories, UriInfo uriInfo) {
    this.categories = categories.stream().map(c -> c.withLinks(uriInfo))
        .collect(Collectors.toList());
  }

}
