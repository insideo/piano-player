import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const client = axios.create({
    baseURL: process.env.VUE_APP_API,
    timeout: 30000,
})

client.defaults.headers.common['Accept'] = 'application/json'
client.defaults.headers.put['Content-Type'] = 'application/json'
client.defaults.headers.post['Content-Type'] = 'application/json'

const TAB_NOW_PLAYING = 1

export default new Vuex.Store({
  state: {
    api: {},
    composers: [],
    performers: [],
    categories: [],
    expandedComposers: {},
    expandedPerformers: {},
    expandedCategories: {},
    composerSongs: {},
    performerSongs: {},
    categorySongs: {},
    playlist: {
      tracks: []
    },
    nowPlaying: {},
    mixerVolume: 0.0,
    mixerRemote: null,
    init: false,
    activeTab: 0
  },
  mutations: {
    updateApi(state, api) {
      state.api = api
    },
    updateActiveTab(state, index) {
      state.activeTab = index
    },
    updateComposers(state, composers) {
      state.composers = composers
      state.expandedComposers = {}
    },
    updatePerformers(state, performers) {
      state.performers = performers
      state.expandedPerformers = {}
    },
    updateCategories(state, categories) {
      state.categories = categories
      state.expandedCategories = {}
    },
    updateMixerVolume(state, mixerVolume) {
      state.mixerVolume = mixerVolume
    },
    updateMixerRemote(state, handle) {
      state.mixerRemote = handle
    },
    updatePlaylist(state, playlist) {
      state.playlist = playlist
    },
    updateNowPlaying(state, nowPlaying) {
      state.nowPlaying = nowPlaying
    },
    setInit(state, init) {
      state.init = init
    },
    removeSongsByComposer(state, id) {
      delete state.composerSongs[id]
    },
    removeSongsByPerformer(state, id) {
      delete state.performerSongs[id]
    },
    removeSongsByCategory(state, id) {
      delete state.categorySongs[id]
    },
    addSongsByComposer(state, payload) {
      Vue.set(state.composerSongs, payload.id, payload.content)
    },
    addSongsByPerformer(state, payload) {
      Vue.set(state.performerSongs, payload.id, payload.content)
    },
    addSongsByCategory(state, payload) {
      Vue.set(state.categorySongs, payload.id, payload.content)
    },
    toggleExpandedComposer(state, id) {
      Vue.set(state.expandedComposers, id, !state.expandedComposers[id])
    },
    toggleExpandedPerformer(state, id) {
      Vue.set(state.expandedPerformers, id, !state.expandedPerformers[id])
    },
    toggleExpandedCategory(state, id) {
      Vue.set(state.expandedCategories, id, !state.expandedCategories[id])
    }
  },
  actions: {
    init({commit, dispatch}) {
      commit('setInit', false)
      client.get(this.state.apiBase)
        .then((response) => {
          commit('updateApi', response.data)
          return client.get(this.state.api.links.composer)
        })
        .then((response) => {
          commit('updateComposers', response.data.composers)
          return client.get(this.state.api.links.performer)
        })
        .then((response) => {
          commit('updatePerformers', response.data.performers)
          return client.get(this.state.api.links.category)
        })
        .then((response) => {
          commit('updateCategories', response.data.categories)
          return client.get(this.state.api.links.playlist)
        })
        .then((response) => {
          commit('updatePlaylist', response.data)
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
          return client.get(this.state.api.links.mixer)
        })
        .then((response) => {
          commit('updateMixerVolume', response.data.volume)
          setInterval(() => { dispatch("refreshUi") }, 1000)
          commit('setInit', true)
          if (this.state.playlist.playing) {
            commit('updateActiveTab', TAB_NOW_PLAYING)
          }
        })
    },
    refreshUi({commit}) {
      client.get(this.state.api.links.playlist)
        .then((response) => {
          commit('updatePlaylist', response.data)
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
          return client.get(this.state.api.links.mixer)
        })
        .then((response) => {
          commit('updateMixerVolume', response.data.volume)
        })
    },
    loadSongsByComposer({commit}, id) {
      this.state.composers.filter(c => c.id == id).forEach(c => {
        client.get(c.links.self).then((response) => {
          commit('addSongsByComposer', { id: id, content: response.data.tracks})
        })
      })
    },
    loadSongsByPerformer({commit}, id) {
      this.state.performers.filter(p => p.id == id).forEach(p => {
        client.get(p.links.self).then((response) => {
          commit('addSongsByPerformer', { id: id, content: response.data.tracks})
        })
      })
    },
    loadSongsByCategory({commit}, id) {
      this.state.categories.filter(c => c.id == id).forEach(c => {
        client.get(c.links.self).then((response) => {
          commit('addSongsByCategory', { id: id, content: response.data.tracks})
        })
      })
    },
    enqueueComposer({commit}, id) {
      this.state.composers.filter(c => c.id == id).forEach(c => {
        client.post(c.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playComposer({commit}, id) {
      this.state.composers.filter(c => c.id == id).forEach(c => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(c.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    enqueueTrackByComposer({commit}, sel) {
      this.state.composerSongs[sel.composerId].filter(t => t.id == sel.trackId).forEach(t => {
        client.post(t.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playTrackByComposer({commit}, sel) {
      this.state.composerSongs[sel.composerId].filter(t => t.id == sel.trackId).forEach(t => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(t.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    enqueueCategory({commit}, id) {
      this.state.categories.filter(c => c.id == id).forEach(c => {
        client.post(c.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playCategory({commit}, id) {
      this.state.categories.filter(c => c.id == id).forEach(c => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(c.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    enqueueTrackByCategory({commit}, sel) {
      this.state.categorySongs[sel.categoryId].filter(t => t.id == sel.trackId).forEach(t => {
        client.post(t.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playTrackByCategory({commit}, sel) {
      this.state.categorySongs[sel.categoryId].filter(t => t.id == sel.trackId).forEach(t => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(t.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    enqueuePerformer({commit}, id) {
      this.state.performers.filter(p => p.id == id).forEach(p => {
        client.post(p.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playPerformer({commit}, id) {
      this.state.performers.filter(p => p.id == id).forEach(p => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(p.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    enqueueTrackByPerformer({commit}, sel) {
      this.state.performerSongs[sel.performerId].filter(t => t.id == sel.trackId).forEach(t => {
        client.post(t.links.enqueue)
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
          })
      })
    },
    playTrackByPerformer({commit}, sel) {
      this.state.performerSongs[sel.performerId].filter(t => t.id == sel.trackId).forEach(t => {
        client.delete(this.state.api.links.playlist)
          .then(() => {
            return client.post(t.links.enqueue)
          })
          .then(() => {
            return client.get(this.state.api.links.playlist)
          })
          .then((response) => {
            commit('updatePlaylist', response.data)
            return client.post(this.state.nowPlaying.links.play)
          })
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
            commit('updateActiveTab', TAB_NOW_PLAYING)
          })
      })
    },
    shufflePlaylist({commit}) {
      client.post(this.state.playlist.links.shuffle)
        .then(() => {
          return client.get(this.state.api.links.playlist)
        })
        .then((response) => {
          commit('updatePlaylist', response.data)
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    emptyPlaylist({commit}) {
      client.delete(this.state.api.links.playlist)
        .then(() => {
          return client.get(this.state.api.links.playlist)
        })
        .then((response) => {
          commit('updatePlaylist', response.data)
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    seekPrev({commit}) {
      client.post(this.state.nowPlaying.links.prev)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    seekNext({commit}) {
      client.post(this.state.nowPlaying.links.next)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    back30({commit}) {
      client.post(this.state.nowPlaying.links.back30)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    forward30({commit}) {
      client.post(this.state.nowPlaying.links.forward30)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    seekTrack({commit}, id) {
      this.state.playlist.tracks.filter(t => t.id == id).forEach(t => {
        client.post(t.links.seek)
          .then(() => {
            return client.get(this.state.api.links.nowplaying)
          })
          .then((response) => {
            commit('updateNowPlaying', response.data)
          })
      })
    },
    play({commit}) {
      client.post(this.state.nowPlaying.links.play)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    stop({commit}) {
      client.post(this.state.nowPlaying.links.stop)
        .then(() => {
          return client.get(this.state.api.links.nowplaying)
        })
        .then((response) => {
          commit('updateNowPlaying', response.data)
        })
    },
    saveMixerVolume({commit}, volume) {
      commit('updateMixerVolume', volume)
      if (this.state.mixerRemote != null) {
        clearTimeout(this.state.mixerRemote)
        commit('updateMixerRemote', null)
      }
      var handle = setTimeout(() => {
        client.put(this.state.api.links.mixer, { volume: this.state.mixerVolume })
          .then(() => {
            return client.get(this.state.api.links.mixer)
          })
          .then((response) => {
            commit('updateMixerVolume', response.data.volume)
          })
      }, 100)
      commit('updateMixerRemote', handle)
    }
  }
})
