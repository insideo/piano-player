package org.randomcoder.piano.model;

import jakarta.ws.rs.core.UriInfo;

import java.util.List;
import java.util.stream.Collectors;

public class Performers {

  public List<Performer> performers;

  public Performers(List<Performer> performers, UriInfo uriInfo) {
    this.performers = performers.stream().map(c -> c.withLinks(uriInfo))
        .collect(Collectors.toList());
  }

}
