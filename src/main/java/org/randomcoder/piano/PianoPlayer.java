package org.randomcoder.piano;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http2.server.HTTP2CServerConnectionFactory;
import org.eclipse.jetty.rewrite.handler.HeaderPatternRule;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.rewrite.handler.Rule;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.ForwardedRequestCustomizer;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.SecuredRedirectHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;
import org.randomcoder.piano.config.Config;
import org.randomcoder.piano.library.FileSystemTrackLibrary;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.mixer.DefaultMixer;
import org.randomcoder.piano.mixer.Mixer;
import org.randomcoder.piano.player.DefaultPlayer;
import org.randomcoder.piano.player.Player;
import org.randomcoder.piano.playlist.DefaultPlaylistState;
import org.randomcoder.piano.playlist.PlaylistState;
import org.randomcoder.piano.util.CalibratePiano;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class PianoPlayer {

  private static final Logger LOG = LoggerFactory.getLogger(PianoPlayer.class);

  private final TrackLibrary trackLibrary;
  private final Mixer mixer;
  private final PlaylistState playlistState;
  private final Player player;
  private final Server server;

  public PianoPlayer(Config config) throws Exception {

    LOG.info("Starting web server on {}:{} using document root {}",
        config.bindAddress, config.bindPort, contentBase(this));

    trackLibrary = trackLibrary(config);
    mixer = mixer(config);
    playlistState = playlistState();
    player = player(config, playlistState, mixer);
    server =
        createServer(this, config, trackLibrary, mixer, playlistState, player);
  }

  static void addHttpConnector(Config config, HttpConfiguration httpConfig,
      Server server) {
    var connectionFactories = new ArrayList<>();

    // http/1.1 connector
    connectionFactories.add(new HttpConnectionFactory(httpConfig));

    // h2c connector
    var http2cFactory = new HTTP2CServerConnectionFactory(httpConfig);
    http2cFactory.setMaxConcurrentStreams(-1);
    http2cFactory.setInitialStreamRecvWindow(65535);
    connectionFactories.add(http2cFactory);

    var httpConnector = new ServerConnector(server, 1, -1,
        connectionFactories.toArray(new ConnectionFactory[] {}));
    httpConnector.setHost(
        "*".equals(config.bindAddress) ? "0.0.0.0" : config.bindAddress);
    httpConnector.setPort(config.bindPort);

    server.addConnector(httpConnector);
  }

  static String contentBase(Object owner) {
    return owner.getClass()
        .getResource("/piano-player-resources-placeholder.txt").toExternalForm()
        .replaceAll("/piano-player-resources-placeholder\\.txt$", "/webapp/");
  }

  static Server createServer(Object owner, Config config,
      TrackLibrary trackLibrary, Mixer mixer, PlaylistState playlistState,
      Player player) throws IOException {

    var server = new Server(threadPool(config));
    server.addBean(new ScheduledExecutorScheduler());

    var httpConfig = httpConfiguration(config);
    var handlers = new HandlerCollection();
    var rootContext = rootContext(owner);
    var resourceConfig =
        resourceConfig(owner, trackLibrary, mixer, playlistState, player);

    configureForward(config, httpConfig);
    configureSendSts(config, handlers);
    configureForceHttps(config, handlers);

    addJerseyContainer(rootContext, resourceConfig);
    addDefaultServlet(rootContext);

    handlers.addHandler(rootContext);
    server.setHandler(handlers);

    addHttpConnector(config, httpConfig, server);

    return server;
  }

  static HttpConfiguration httpConfiguration(Config config) {
    var httpConfig = new HttpConfiguration();
    httpConfig.setSecurePort(config.securePort);
    return httpConfig;
  }

  static ServletContextHandler rootContext(Object owner)
      throws MalformedURLException, IOException {
    var root = new ServletContextHandler();
    root.setContextPath("/");
    root.setBaseResource(Resource.newResource(contentBase(owner)));
    root.setWelcomeFiles(new String[] { "index.html" });
    root.setAllowNullPathInfo(true);
    return root;
  }

  static TrackLibrary trackLibrary(Config config) throws IOException {
    var library = new FileSystemTrackLibrary(config.libraryPath,
        config.libraryMetadataCache, config.doorbellMidi,
        config.doorbellVolume);
    library.refresh();
    return library;
  }

  static Mixer mixer(Config config) throws IOException {
    var mixer = new DefaultMixer(config.volumeDefault);
    return mixer;
  }

  static PlaylistState playlistState() throws IOException {
    var playlist = new DefaultPlaylistState();
    return playlist;
  }

  static Player player(Config config, PlaylistState playlistState, Mixer mixer)
      throws IOException {
    var player = new DefaultPlayer(config.midiDevice, playlistState, mixer,
        config.minKeyVelocities, config.volumeMin, config.volumeMax,
        config.volumeDefault, config.maxKeyVelocity, config.velocityPivotPoint,
        config.velocityLowRange);

    return player;
  }

  static ResourceConfig resourceConfig(Object owner, TrackLibrary trackLibrary,
      Mixer mixer, PlaylistState playlistState, Player player)
      throws IOException {

    var basePackage = owner.getClass().getPackageName();

    var resourceConfig = new ResourceConfig();
    resourceConfig.property(ServerProperties.WADL_FEATURE_DISABLE, true);
    resourceConfig.register(new AbstractBinder() {
      @Override protected void configure() {
        bind(trackLibrary).to(TrackLibrary.class);
        bind(mixer).to(Mixer.class);
        bind(playlistState).to(PlaylistState.class);
        bind(player).to(Player.class);
      }
    });
    resourceConfig.packages(basePackage + ".providers");
    resourceConfig.packages(basePackage + ".resources");

    return resourceConfig;
  }

  static void configureForward(Config config, HttpConfiguration httpConfig) {
    if (config.forward) {
      httpConfig.addCustomizer(new ForwardedRequestCustomizer());
    }
  }

  static void configureSendSts(Config config, HandlerCollection handlers) {
    if (config.sendSts) {
      var handler = new RewriteHandler();
      var stsRule = new HeaderPatternRule();
      stsRule.setPattern("/*");
      stsRule.setName("Strict-Transport-Security");
      stsRule.setValue("max-age=" + new DecimalFormat("####################")
          .format(config.stsMaxAge));
      handler.setRules(new Rule[] { stsRule });
      handlers.addHandler(handler);
    }
  }

  static void configureForceHttps(Config config, HandlerCollection handlers) {
    if (config.forceHttps) {
      handlers.addHandler(new SecuredRedirectHandler() {
        @Override public void handle(String target, Request baseRequest,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

          // need to allow doorbell code in
          if (request.getRequestURI().startsWith("/api/v1/doorbell")) {
            return;
          }
          super.handle(target, baseRequest, request, response);
        }
      });
    }
  }

  static void addJerseyContainer(ServletContextHandler rootContext,
      ResourceConfig resourceConfig) {
    var jerseyContainer = new ServletContainer(resourceConfig);
    var jerseyHolder = new ServletHolder(jerseyContainer);
    rootContext.addServlet(jerseyHolder, "/api/v1/*");
  }

  static void addDefaultServlet(ServletContextHandler rootContext) {
    var defaultHolder = new ServletHolder("default", DefaultServlet.class);
    defaultHolder.setInitParameter("dirAllowed", "true");
    defaultHolder.setInitParameter("redirectWelcome", "false");
    defaultHolder.setInitParameter("precompressed", "gzip=.gz");
    defaultHolder.setInitParameter("etags", "true");
    rootContext.addServlet(defaultHolder, "/");
  }

  public void start() throws Exception {
    player.startup();
    server.start();
  }

  public void stop() {
    try {
      server.stop();
      player.shutdown();
    } catch (Exception e) {
      LOG.error("Exception during stop", e);
    }
  }

  static QueuedThreadPool threadPool(Config config) {
    return new QueuedThreadPool(config.threadPoolMax, config.threadPoolMin,
        config.threadPoolIdleTimeoutMs);
  }

  static void redirectJulLogging(java.util.logging.Level level)
      throws Exception {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
    java.util.logging.Logger.getLogger("").setLevel(level);
  }

  public static void usage() {
    System.err.println("Usage: " + PianoPlayer.class.getCanonicalName()
        + " <config-file> [calibrate]");
    System.err.flush();
  }

  public static void main(String[] args) throws Exception {
    if (args.length == 0) {
      LOG.warn("No explicit configuration given, using development mode.");
    } else if (args.length < 1) {
      usage();
      System.exit(1);
    }

    Config config = Config.parse(
        args.length == 0 ? PianoPlayer.class.getResource("/config/dev.yml") :
            Paths.get(args[0]).toUri().toURL());

    LOG.info("Configuration: {}", config);

    redirectJulLogging(config.julLogLevel);

    long startTime = System.currentTimeMillis();

    if (args.length >= 2 && "calibrate".equals(args[1])) {
      CalibratePiano c = new CalibratePiano(config.midiDevice);
      c.run(Arrays.stream(args).skip(2).collect(Collectors.toList()));
      System.exit(0);
    }

    PianoPlayer site = new PianoPlayer(config);
    Runtime.getRuntime().addShutdownHook(new Thread(site::stop));
    site.start();

    long endTime = System.currentTimeMillis();
    long elapsedTime = endTime - startTime;

    LOG.info("Piano player started in {}ms", elapsedTime);
  }

}
