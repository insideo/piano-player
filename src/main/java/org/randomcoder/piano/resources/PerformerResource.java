package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.Performer;
import org.randomcoder.piano.model.Performers;
import org.randomcoder.piano.playlist.PlaylistState;

@Path("/library/performer") public class PerformerResource {

  @Inject public TrackLibrary library;

  @Inject public PlaylistState playlistState;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Performers listPerformers(@Context UriInfo uriInfo) {
    return new Performers(library.listPerformers(), uriInfo);
  }

  @GET @Path("{id}") @Produces(MediaType.APPLICATION_JSON)
  public Performer getPerformer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    return library.fetchPerformer(id).withLinks(uriInfo);
  }

  @POST @Path("{id}/enqueue!")
  public Response enqueuePerformer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.add(library.fetchPerformer(id).tracks);
    return Response.noContent().build();
  }

  @POST @Path("{id}/dequeue!")
  public Response dequeuePerformer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.remove(library.fetchPerformer(id).tracks);
    return Response.noContent().build();
  }

}
