package org.randomcoder.piano.model;

import jakarta.ws.rs.core.UriInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Composers {

  public List<Composer> composers;

  public Composers() {
    this.composers = new ArrayList<>();
  }

  public Composers(List<Composer> composers, UriInfo uriInfo) {
    this.composers = composers.stream().map(c -> c.withLinks(uriInfo))
        .collect(Collectors.toList());
  }

}
