package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.Track;
import org.randomcoder.piano.model.TrackLinkContext;
import org.randomcoder.piano.playlist.PlaylistState;

import java.util.Collections;

@Path("/library/track") public class TrackResource {

  @Inject public TrackLibrary library;

  @Inject public PlaylistState playlistState;

  @GET @Path("{id}") @Produces(MediaType.APPLICATION_JSON)
  public Track getTrack(@PathParam("id") String id, @Context UriInfo uriInfo) {
    return library.fetchTrack(id).withLinks(uriInfo, TrackLinkContext.LIBRARY);
  }

  @POST @Path("{id}/enqueue!")
  public Response enqueueTrack(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.add(Collections.singletonList(library.fetchTrack(id)));
    return Response.noContent().build();
  }

  @POST @Path("{id}/dequeue!")
  public Response dequeueTrack(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.remove(Collections.singletonList(library.fetchTrack(id)));
    return Response.noContent().build();
  }

}
