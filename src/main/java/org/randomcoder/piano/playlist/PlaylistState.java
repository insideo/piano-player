package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;
import org.randomcoder.piano.model.Track;

import java.util.Collection;
import java.util.function.Consumer;

public interface PlaylistState {

  public Playlist getPlaylist();

  public void clear();

  public void shuffle();

  public void add(Collection<Track> tracks);

  public void remove(Collection<Track> tracks);

  public void addPlaylistClearedListener(
      Consumer<PlaylistClearedEvent> listener);

  public void addPlaylistShuffledListener(
      Consumer<PlaylistShuffledEvent> listener);

  public void addPlaylistTracksAddedListener(
      Consumer<PlaylistTracksAddedEvent> listener);

  public void addPlaylistTracksRemovedListener(
      Consumer<PlaylistTracksRemovedEvent> listener);

}
