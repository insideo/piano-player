package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;
import org.randomcoder.piano.model.Track;

import java.util.List;

public class PlaylistTracksRemovedEvent {

  private final Playlist before;
  private final Playlist after;
  private final List<Track> removed;

  public PlaylistTracksRemovedEvent(Playlist before, Playlist after,
      List<Track> removed) {
    this.before = before;
    this.after = after;
    this.removed = removed;
  }

  public Playlist getBefore() {
    return before;
  }

  public Playlist getAfter() {
    return after;
  }

  public List<Track> getRemoved() {
    return removed;
  }

}
