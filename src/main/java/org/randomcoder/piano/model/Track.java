package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(value = Include.NON_NULL) public class Track
    implements Comparable<Track> {

  public String id;

  @JsonIgnore public Path filename;

  public String title;

  @JsonIgnore public String sortTitle;

  public Composer composer;
  public Performer performer;
  public Category category;
  public Long lengthMicros;

  public Integer performanceYear;

  @JsonInclude(value = Include.NON_EMPTY) public Map<String, URI> links =
      new HashMap<>();

  public Track() {

  }

  public Track(String id) {
    this.id = id;
  }

  public Track(String id, Path filename, String title, String sortTitle,
      Composer composer, Performer performer, Category category,
      Integer performanceYear, Long lengthMicros) {
    this.id = id;
    this.filename = filename;
    this.title = title;
    this.sortTitle = (sortTitle == null) ? title : sortTitle;
    this.composer = composer;
    this.performer = performer;
    this.category = category;
    this.performanceYear = performanceYear;
    this.lengthMicros = lengthMicros;
  }

  @Override public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Track other = (Track) obj;
    return Objects.equals(id, other.id);
  }

  @Override public int compareTo(Track o) {
    return Objects.compare(sortTitle, o.sortTitle, String::compareTo);
  }

  public Track withLinks(UriInfo info, TrackLinkContext linkContext) {
    Track clone = new Track(id, filename, title, sortTitle,
        composer == null ? null : composer.withLinks(info),
        performer == null ? null : performer.withLinks(info),
        category == null ? null : category.withLinks(info), performanceYear,
        lengthMicros);

    clone.links.put("self",
        info.getBaseUriBuilder().path("/library/track/{id}").build(id));
    switch (linkContext) {
    case LIBRARY:
      clone.links.put("enqueue",
          info.getBaseUriBuilder().path("/library/track/{id}/enqueue!")
              .build(id));
      clone.links.put("dequeue",
          info.getBaseUriBuilder().path("/library/track/{id}/dequeue!")
              .build(id));
      break;
    case NOW_PLAYING:
      clone.links.put("seek",
          info.getBaseUriBuilder().path("/now-playing/seek!/{id}").build(id));
      break;
    }
    return clone;
  }

}
