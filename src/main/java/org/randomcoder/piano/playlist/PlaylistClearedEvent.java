package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;

public class PlaylistClearedEvent {

  private final Playlist before;

  public PlaylistClearedEvent(Playlist before) {
    this.before = before;
  }

  public Playlist getBefore() {
    return before;
  }

}
