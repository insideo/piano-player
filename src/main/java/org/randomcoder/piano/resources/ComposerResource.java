package org.randomcoder.piano.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.randomcoder.piano.library.TrackLibrary;
import org.randomcoder.piano.model.Composer;
import org.randomcoder.piano.model.Composers;
import org.randomcoder.piano.playlist.PlaylistState;

@Path("/library/composer") public class ComposerResource {

  @Inject public TrackLibrary library;

  @Inject public PlaylistState playlistState;

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Composers listComposers(@Context UriInfo uriInfo) {
    return new Composers(library.listComposers(), uriInfo);
  }

  @GET @Path("{id}") @Produces(MediaType.APPLICATION_JSON)
  public Composer getComposer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    return library.fetchComposer(id).withLinks(uriInfo);
  }

  @POST @Path("{id}/enqueue!")
  public Response enqueueComposer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.add(library.fetchComposer(id).tracks);
    return Response.noContent().build();
  }

  @POST @Path("{id}/dequeue!")
  public Response dequeueComposer(@PathParam("id") String id,
      @Context UriInfo uriInfo) {
    playlistState.remove(library.fetchComposer(id).tracks);
    return Response.noContent().build();
  }

}
