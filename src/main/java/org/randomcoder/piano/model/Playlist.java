package org.randomcoder.piano.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@JsonInclude(value = Include.NON_NULL) public class Playlist {

  public List<Track> tracks = new ArrayList<>();

  @JsonInclude(value = Include.NON_EMPTY) public Map<String, URI> links =
      new HashMap<>();

  public Playlist() {
  }

  public Playlist withLinks(UriInfo info) {
    Playlist clone = new Playlist();
    clone.tracks.addAll(tracks.stream()
        .map(c -> c.withLinks(info, TrackLinkContext.NOW_PLAYING))
        .collect(Collectors.toList()));
    clone.links.put("self", info.getBaseUriBuilder().path("/playlist").build());
    clone.links.put("shuffle",
        info.getBaseUriBuilder().path("/playlist/shuffle!").build());
    return clone;
  }

  @Override public Playlist clone() {
    Playlist clone = new Playlist();
    clone.tracks.addAll(tracks);
    return clone;
  }

}
