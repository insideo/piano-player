package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;

public class PlaylistShuffledEvent {

  private final Playlist before;
  private final Playlist after;

  public PlaylistShuffledEvent(Playlist before, Playlist after) {
    this.before = before;
    this.after = after;
  }

  public Playlist getBefore() {
    return before;
  }

  public Playlist getAfter() {
    return after;
  }

}
