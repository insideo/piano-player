package org.randomcoder.piano.mixer;

import java.util.function.Consumer;

public interface Mixer {

  public double getVolume();

  public void setVolume(double value);

  public void addVolumeChangedListener(Consumer<VolumeChangedEvent> listener);

}
