package org.randomcoder.piano.playlist;

import org.randomcoder.piano.model.Playlist;
import org.randomcoder.piano.model.Track;

import java.util.List;

public class PlaylistTracksAddedEvent {

  private final Playlist before;
  private final Playlist after;
  private final List<Track> added;

  public PlaylistTracksAddedEvent(Playlist before, Playlist after,
      List<Track> added) {
    this.before = before;
    this.after = after;
    this.added = added;
  }

  public Playlist getBefore() {
    return before;
  }

  public Playlist getAfter() {
    return after;
  }

  public List<Track> getAdded() {
    return added;
  }

}
