package org.randomcoder.piano.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = false) public class MidiDeviceDescriptor {

  private static final Logger LOG =
      LoggerFactory.getLogger(MidiDeviceDescriptor.class);

  public String name;
  public String vendor;
  public String description;
  public String version;

  private static boolean match(String type, Pattern pattern, String match) {
    boolean result = pattern.matcher(match == null ? "" : match).matches();
    LOG.debug("Matching {} of value '{}' against pattern '{}': {}", type, match,
        pattern, result);
    return result;
  }

  @JsonIgnore public MidiDevice create() {
    var namePat = Pattern.compile(name == null ? "^.*$" : name);
    var vendorPat = Pattern.compile(vendor == null ? "^.*$" : vendor);
    var descPat = Pattern.compile(description == null ? "^.*$" : description);
    var versionPat = Pattern.compile(version == null ? "^.*$" : version);

    var devices = Arrays.stream(MidiSystem.getMidiDeviceInfo()).map(di -> {
      try {
        return MidiSystem.getMidiDevice(di);
      } catch (Exception e) {
        return null;
      }
    }).filter(Objects::nonNull).filter(d -> d.getMaxReceivers() != 0)
        .filter(d -> match("name", namePat, d.getDeviceInfo().getName()))
        .filter(d -> match("vendor", vendorPat, d.getDeviceInfo().getVendor()))
        .filter(d -> match("description", descPat,
            d.getDeviceInfo().getDescription())).filter(
            d -> match("version", versionPat, d.getDeviceInfo().getVersion()))
        .collect(Collectors.toList());

    if (devices.isEmpty()) {
      throw new IllegalArgumentException("No MIDI devices match.");
    }

    if (devices.size() > 1) {
      throw new IllegalArgumentException("Multiple MIDI devices match.");
    }

    return devices.get(0);
  }

  @Override public String toString() {
    return String
        .format("{ name=%s, vendor=%s, description=%s, version=%s }", name,
            vendor, description, version);
  }

}
