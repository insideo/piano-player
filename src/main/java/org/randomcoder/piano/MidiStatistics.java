package org.randomcoder.piano;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.Track;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MidiStatistics {

  public static void usage() {
    System.err.printf("Usage: %s <file.mid> [file2.mid...]%n",
        MidiStatistics.class.getName());
  }

  public static void main(String[] args) throws Exception {

    if (args.length < 1) {
      usage();
      System.exit(1);
    }

    List<Path> paths =
        Arrays.stream(args).map(Paths::get).collect(Collectors.toList());

    Sequencer sequencer = MidiSystem.getSequencer(false);

    long files = 0;
    long tracks = 0;
    long sysexMessages = 0;
    long shortMessages = 0;
    long otherMessages = 0;
    long totalMessages = 0;
    long noteOn = 0;
    long noteOff = 0;
    long cc = 0;
    long clock = 0;
    long activeSensing = 0;
    long channelPressure = 0;
    long shortContinue = 0;
    long endOfExclusive = 0;
    long timeCode = 0;
    long pitchBend = 0;
    long polyPressure = 0;
    long programChange = 0;
    long otherShort = 0;
    long ccs[] = new long[128];
    long notesOn[] = new long[128];
    long noteOnVel[] = new long[128];
    long notesOff[] = new long[128];
    long noteOffVel[] = new long[128];

    for (Path path : paths) {
      files++;
      System.err.printf("Analyzing %s...%n", path);
      try (var in = Files.newInputStream(path)) {
        sequencer.setSequence(in);
      }
      var sequence = sequencer.getSequence();
      for (Track track : sequence.getTracks()) {
        tracks++;
        for (int i = 0; i < track.size(); i++) {
          totalMessages++;
          var msg = track.get(i).getMessage();
          if (msg instanceof SysexMessage) {
            sysexMessages++;
          } else if (msg instanceof ShortMessage) {
            shortMessages++;
            var sm = (ShortMessage) msg;
            if (sm.getCommand() == ShortMessage.NOTE_ON) {
              if (sm.getData2() == 0) {
                noteOff++;
                notesOff[sm.getData1()]++;
                noteOffVel[0]++;
                continue;
              }
              noteOn++;
              notesOn[sm.getData1()]++;
              noteOnVel[sm.getData2()]++;
            } else if (sm.getCommand() == ShortMessage.NOTE_OFF) {
              noteOff++;
              notesOff[sm.getData1()]++;
              noteOffVel[sm.getData2()]++;
            } else if (sm.getCommand() == ShortMessage.CONTROL_CHANGE) {
              cc++;
              ccs[sm.getData1()]++;
            } else if (sm.getCommand() == ShortMessage.TIMING_CLOCK) {
              clock++;
            } else if (sm.getCommand() == ShortMessage.ACTIVE_SENSING) {
              activeSensing++;
            } else if (sm.getCommand() == ShortMessage.CHANNEL_PRESSURE) {
              channelPressure++;
            } else if (sm.getCommand() == ShortMessage.CONTINUE) {
              shortContinue++;
            } else if (sm.getCommand() == ShortMessage.END_OF_EXCLUSIVE) {
              endOfExclusive++;
            } else if (sm.getCommand() == ShortMessage.MIDI_TIME_CODE) {
              timeCode++;
            } else if (sm.getCommand() == ShortMessage.PITCH_BEND) {
              pitchBend++;
            } else if (sm.getCommand() == ShortMessage.POLY_PRESSURE) {
              polyPressure++;
            } else if (sm.getCommand() == ShortMessage.PROGRAM_CHANGE) {
              programChange++;
            } else {
              otherShort++;
            }
          } else {
            otherMessages++;
          }
        }
      }
    }

    System.err.println("Done.");
    System.err.println();
    System.err.flush();

    System.out.println("Statistics:");
    System.out.printf("     Files: %d%n", files);
    System.out.printf("    Tracks: %d%n", tracks);
    System.out.printf("  Messages: %d%n", totalMessages);
    System.out.printf("       SysEx: %d%n", sysexMessages);
    System.out.printf("       Short: %d%n", shortMessages);
    System.out.printf("           Active Sensing: %d%n", activeSensing);
    System.out.printf("         Channel Pressure: %d%n", channelPressure);
    System.out.printf("                 Continue: %d%n", shortContinue);
    System.out.printf("           Control Change: %d%n", cc);
    System.out.printf("         End of Exclusive: %d%n", endOfExclusive);
    System.out.printf("           MIDI Time Code: %d%n", timeCode);
    System.out.printf("                  Note On: %d%n", noteOn);
    System.out.printf("                 Note Off: %d%n", noteOff);
    System.out.printf("               Pitch Bend: %d%n", pitchBend);
    System.out.printf("            Poly Pressure: %d%n", polyPressure);
    System.out.printf("           Program Change: %d%n", programChange);
    System.out.printf("             Timing Clock: %d%n", clock);
    System.out.printf("                    Other: %d%n", otherShort);
    System.out.printf("       Other: %d%n", otherMessages);

    System.out.println();
    System.out.println("CC breakdown:");
    for (int i = 0; i < 128; i++) {
      if (ccs[i] != 0) {
        System.out.printf("  CC %3d | %7d%n", i, ccs[i]);
      }
    }

    System.out.println();
    System.out.println("Note breakdown:");
    for (int i = 0; i < 128; i++) {
      if (notesOn[i] != 0 || notesOff[i] != 0) {
        System.out.printf("  Note %3d | ON: %7d | OFF: %7d%n", i, notesOn[i],
            notesOff[i]);
      }
    }

    System.out.println();
    System.out.println("Velocity breakdown:");
    for (int i = 0; i < 128; i++) {
      if (noteOnVel[i] != 0 || noteOffVel[i] != 0) {
        System.out
            .printf("  Velocity %3d | ON: %7d %6.2f%% | OFF: %7d %6.2f%%%n", i,
                noteOnVel[i], ((double) noteOnVel[i] / (double) noteOn) * 100d,
                noteOffVel[i],
                ((double) noteOffVel[i] / (double) noteOff) * 100d);
      }
    }

    // quantize velocity values
    long qNoteOnVel[] = new long[16];
    long qNoteOffVel[] = new long[16];

    for (int i = 0; i < 16; i++) {
      for (int j = 0; j < 8; j++) {
        qNoteOnVel[i] += noteOnVel[i * 8 + j];
        qNoteOffVel[i] += noteOffVel[i * 8 + j];
      }
    }

    System.out.println();
    System.out.println("Quantized velocity breakdown:");
    for (int i = 0; i < qNoteOnVel.length; i++) {
      if (qNoteOnVel[i] != 0 || qNoteOffVel[i] != 0) {
        System.out
            .printf("  Velocity %3d-%3d | ON: %7d %6.2f%% | OFF: %7d %6.2f%%%n",
                i * 8, i * 8 + 7, qNoteOnVel[i],
                ((double) qNoteOnVel[i] / (double) noteOn) * 100d,
                qNoteOffVel[i],
                ((double) qNoteOffVel[i] / (double) noteOff) * 100d);
      }
    }

  }

}
