package org.randomcoder.piano.config;

public class KeyCalibration {

  public int key;
  public int minVelocity;

  public KeyCalibration() {
  }

  public KeyCalibration(int key, int minVelocity) {
    this.key = key;
    this.minVelocity = minVelocity;
  }
}
