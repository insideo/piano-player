package org.randomcoder.piano.resources;

import org.randomcoder.piano.model.Api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

@Path("") public class ApiResource {

  @GET @Produces(MediaType.APPLICATION_JSON)
  public Api getApi(@Context UriInfo uriInfo) {
    return new Api(uriInfo);
  }

}
