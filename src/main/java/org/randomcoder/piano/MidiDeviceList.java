package org.randomcoder.piano;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import java.util.Arrays;
import java.util.Objects;

public class MidiDeviceList {

  public static void main(String[] args) throws Exception {
    System.out.printf("midiDevices:%n");

    Arrays.stream(MidiSystem.getMidiDeviceInfo()).map(di -> {
      try {
        return MidiSystem.getMidiDevice(di);
      } catch (Exception e) {
        return null;
      }
    }).filter(Objects::nonNull).filter(d -> d.getMaxReceivers() != 0)
        .map(MidiDevice::getDeviceInfo).forEach(di -> System.out.printf(
        "  - name: '%s'%n    vendor: '%s'%n    description: '%s'%n    version: '%s'%n",
        di.getName(), di.getVendor(), di.getDescription(), di.getVersion()));
  }

}
